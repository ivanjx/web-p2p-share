# Build.
FROM mcr.microsoft.com/dotnet/sdk:8.0.100-bookworm-slim-amd64 as build
RUN apt-get update && \
    apt-get install -y python3 libatomic1 jq
RUN dotnet workload install wasm-tools
COPY ./web-p2p-share.Shared /web-p2p-share.Shared
COPY ./web-p2p-share /web-p2p-share
WORKDIR /web-p2p-share
RUN dotnet publish \
    -c Release \
    -o /output
# PWA fix.
WORKDIR /output/wwwroot/
COPY ./client.pwa-fix.sh .
RUN chmod +x ./client.pwa-fix.sh && \
    ./client.pwa-fix.sh && \
    rm ./client.pwa-fix.sh

# Runtime.
FROM fholzer/nginx-brotli:v1.24.0
COPY ./client.nginx.conf /etc/nginx/nginx.conf
COPY --from=build /output/wwwroot /usr/share/nginx/html
