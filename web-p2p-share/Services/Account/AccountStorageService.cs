using System;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using web_p2p_share.Shared.Services.Account;

namespace web_p2p_share.Services.Account;

public class AccountStorageService : IAccountStorageService, IAsyncDisposable
{
    bool m_isDisposed;
    IJSInProcessRuntime m_js;
    IJSInProcessObjectReference? m_module;
    IJSInProcessObjectReference? m_handle;

    public AccountStorageService(IJSRuntime js)
    {
        m_js = (IJSInProcessRuntime)js;
    }

    async ValueTask<IJSInProcessObjectReference> LoadModuleAsync()
    {
        if (m_isDisposed)
        {
            throw new ObjectDisposedException(nameof(AccountStorageService));
        }

        if (m_module == null)
        {
            m_module = await m_js.InvokeAsync<IJSInProcessObjectReference>(
                "import",
                "./js/AccountStorage.js");
        }

        return m_module;
    }

    async ValueTask<IJSInProcessObjectReference> OpenDatabaseAsync(IJSInProcessObjectReference module)
    {
        if (m_isDisposed)
        {
            throw new ObjectDisposedException(nameof(AccountStorageService));
        }

        if (m_handle == null)
        {
            m_handle = await module.InvokeAsync<IJSInProcessObjectReference>("open");
        }

        return m_handle;
    }

    public async Task<UserObject?> GetAsync()
    {
        IJSInProcessObjectReference module = await LoadModuleAsync();
        IJSInProcessObjectReference handle = await OpenDatabaseAsync(module);
        UserObject? result = await module.InvokeAsync<UserObject?>("get", handle);

        if (result == null)
        {
            return null;
        }

        return result;
    }

    public async Task SetAsync(UserObject user)
    {
        IJSInProcessObjectReference module = await LoadModuleAsync();
        IJSInProcessObjectReference handle = await OpenDatabaseAsync(module);
        await module.InvokeVoidAsync("set", handle, user);
    }

    public async ValueTask DisposeAsync()
    {
        if (m_handle != null)
        {
            IJSInProcessObjectReference module = await LoadModuleAsync();
            await module.InvokeVoidAsync("close", m_handle);
            await m_handle.DisposeAsync();
            m_handle = null;
        }

        if (m_module != null)
        {
            await m_module.DisposeAsync();
            m_module = null;
        }

        m_isDisposed = true;
    }
}
