using System;
using System.Runtime.InteropServices.JavaScript;
using System.Threading.Tasks;
using web_p2p_share.Shared.Services.Media;

namespace web_p2p_share.Services.Media;

public partial class WakeLockService : IWakeLockService
{
    public static Task InitAsync()
    {
        if (OperatingSystem.IsBrowser())
        {
            return JSHost.ImportAsync(
                "WakeLock",
                "/js/WakeLock.js");
        }

        throw new PlatformNotSupportedException();
    }

    [JSImport("startLock", "WakeLock")]
    public static partial Task _StartLockAsync();

    public void AcquireLock()
    {
        _ = _StartLockAsync();
    }
}
