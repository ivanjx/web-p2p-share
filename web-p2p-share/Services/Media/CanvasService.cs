using System;
using System.Runtime.InteropServices.JavaScript;
using System.Runtime.Versioning;
using System.Threading.Tasks;
using web_p2p_share.Shared.Services.Media;

namespace web_p2p_share.Services.Media;

public partial class CanvasService : ICanvasService
{
    public static Task InitAsync()
    {
        if (OperatingSystem.IsBrowser())
        {
            return JSHost.ImportAsync(
                "Canvas",
                "/js/Canvas.js");
        }
        
        throw new PlatformNotSupportedException();
    }
    
    [JSImport("init", "Canvas")]
    public static partial JSObject _Init(string id);

    [JSImport("getHeight", "Canvas")]
    public static partial double _GetHeight(JSObject initResult);

    [JSImport("getWidth", "Canvas")]
    public static partial double _GetWidth(JSObject initResult);

    [JSImport("draw", "Canvas")]
    public static partial void _Draw(
        JSObject initResult,
        byte[] data);

    public ICanvasHandle Init(string canvasId)
    {
        JSObject handle = _Init(canvasId);
        return new CanvasHandle(handle);
    }

    public double GetHeight(ICanvasHandle handle)
    {
        return _GetHeight(((CanvasHandle)handle).Handle);
    }

    public double GetWidth(ICanvasHandle handle)
    {
        return _GetWidth(((CanvasHandle)handle).Handle);
    }

    public void Draw(ICanvasHandle handle, byte[] data)
    {
        _Draw(((CanvasHandle)handle).Handle, data);
    }

    class CanvasHandle : ICanvasHandle
    {
        JSObject? m_handle;

        public JSObject Handle
        {
            get =>
                m_handle ??
                throw new ObjectDisposedException(nameof(CanvasHandle));
        }

        public CanvasHandle(JSObject handle)
        {
            m_handle = handle;
        }

        public void Dispose()
        {
            if (OperatingSystem.IsBrowser())
            {
                m_handle?.Dispose();
                m_handle = null;
            }
        }
    }
}
