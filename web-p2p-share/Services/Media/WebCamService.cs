using System;
using System.Runtime.InteropServices.JavaScript;
using System.Threading.Tasks;
using web_p2p_share.Shared.Services.Media;

namespace web_p2p_share.Services.Media;

public partial class WebCamService : IWebCamService
{
    public static Task InitAsync()
    {
        if (OperatingSystem.IsBrowser())
        {
            return JSHost.ImportAsync(
                "WebCam",
                "/js/WebCam.js");
        }
        
        throw new PlatformNotSupportedException();
    }

    [JSImport("check", "WebCam")]
    public static partial bool _Check();

    [JSImport("start", "WebCam")]
    public static partial Task<JSObject> _StartAsync(
        string videoId,
        string canvasId);
    
    [JSImport("stop", "WebCam")]
    public static partial void _Stop(
        JSObject initResult);

    [JSImport("getImage", "WebCam")]
    public static partial byte[] _GetImage(
        JSObject initResult);

    public bool IsSupported()
    {
        return _Check();
    }

    public async Task<IWebCamHandle> StartAsync(string videoTagId, string canvasTagId)
    {
        JSObject handle = await _StartAsync(
            videoTagId,
            canvasTagId);
        return new WebCamHandle(handle);
    }

    public void Stop(IWebCamHandle handle)
    {
        _Stop(
            ((WebCamHandle)handle).Handle);
    }

    public byte[] GetImage(IWebCamHandle handle)
    {
        return _GetImage(
            ((WebCamHandle)handle).Handle);
    }

    class WebCamHandle : IWebCamHandle
    {
        JSObject? m_handle;

        public JSObject Handle
        {
            get =>
                m_handle ??
                throw new ObjectDisposedException(nameof(WebCamHandle));
        }

        public WebCamHandle(JSObject handle)
        {
            m_handle = handle;
        }

        public void Dispose()
        {
            if (OperatingSystem.IsBrowser())
            {
                m_handle?.Dispose();
                m_handle = null;
            }
        }
    }
}
