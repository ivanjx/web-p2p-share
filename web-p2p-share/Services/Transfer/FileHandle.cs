using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Forms;
using web_p2p_share.Shared.Services.Transfer;

namespace web_p2p_share.Services.Transfer;

public class FileHandle : IFileHandle
{
    IBrowserFile m_file;
    Stream? m_readStream;

    public string InputId
    {
        get;
    }

    public string Name => m_file.Name;

    public long Size => m_file.Size;

    public Action<FileHandle>? Disposed;

    public FileHandle(
        string inputId,
        IBrowserFile file)
    {
        InputId = inputId;
        m_file = file;
        InitStream();
    }

    void InitStream()
    {
        m_readStream?.Dispose();
        m_readStream = m_file.OpenReadStream(long.MaxValue);
    }

    public async Task<byte[]> ReadAsync(long offset, int count)
    {
        if (m_readStream == null)
        {
            throw new ObjectDisposedException(nameof(FileHandle));
        }

        if (offset < m_readStream.Position)
        {
            // Seek backward. Very rare case.
            InitStream();
        }
        
        if (offset > m_readStream.Position)
        {
            // Seek forward. Rare case too.
            byte[] seekBuff = new byte[1024 * 1024];

            while (m_readStream.Position < offset)
            {
                int toSeekRead = (int)Math.Min(
                    seekBuff.Length,
                    offset - m_readStream.Position);
                await m_readStream.ReadAsync(
                    seekBuff,
                    0,
                    toSeekRead);
            }
        }

        byte[] readBuff = new byte[count];
        int read = await m_readStream.ReadAsync(readBuff);
        byte[] result;
        
        if (read == readBuff.Length)
        {
            result = readBuff;
        }
        else
        {
            result = new byte[read];
            Array.Copy(readBuff, 0, result, 0, read);
        }

        return result;
    }

    public async void Dispose()
    {
        if (m_readStream == null)
        {
            return;
        }

        Disposed?.Invoke(this);
        Stream readStream = m_readStream;
        m_readStream = null;
        await Task.Delay(5000);
        readStream.Dispose();
    }
}
