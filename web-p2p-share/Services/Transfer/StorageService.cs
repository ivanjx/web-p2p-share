using System;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using web_p2p_share.Shared.Services.Transfer;

namespace web_p2p_share.Services.Transfer;

public class StorageService : IStorageService, IAsyncDisposable
{
    bool m_isDisposed;
    IJSInProcessRuntime m_js;
    IJSInProcessObjectReference? m_module;
    IJSInProcessObjectReference? m_handle;

    public StorageService(IJSRuntime js)
    {
        m_js = (IJSInProcessRuntime)js;
    }

    public Task<byte[]> ReadAsync(IFileHandle handle, long offset, int length)
    {
        FileHandle fileHandle = (FileHandle)handle;
        return fileHandle.ReadAsync(offset, length);
    }

    async ValueTask<IJSInProcessObjectReference> LoadModuleAsync()
    {
        if (m_isDisposed)
        {
            throw new ObjectDisposedException(nameof(StorageService));
        }

        if (m_module == null)
        {
            m_module = await m_js.InvokeAsync<IJSInProcessObjectReference>(
                "import",
                "./js/Storage.js");
        }

        return m_module;
    }

    async Task<IJSInProcessObjectReference> GetHandleAsync(IJSInProcessObjectReference module)
    {
        if (m_isDisposed)
        {
            throw new ObjectDisposedException(nameof(StorageService));
        }

        if (m_handle == null)
        {
            m_handle = await module.InvokeAsync<IJSInProcessObjectReference>("open");
        }
        
        return m_handle;
    }


    public async Task<IBlob> GetFileBlobAsync(string fileId)
    {
        IJSInProcessObjectReference module = await LoadModuleAsync();
        IJSInProcessObjectReference handle = await module.InvokeAsync<IJSInProcessObjectReference>(
            "getFileBlob",
            await GetHandleAsync(module),
            fileId);
        return new Blob(handle);
    }

    public async Task DownloadFileBlobAsync(IBlob blobArg, string name)
    {
        IJSInProcessObjectReference module = await LoadModuleAsync();
        Blob blob = (Blob)blobArg;
        await module.InvokeVoidAsync(
            "downloadFileBlob",
            blob.Handle,
            name);
    }

    public async Task<string> InsertChunkAsync(string fileId, long offset, byte[] data)
    {
        IJSInProcessObjectReference module = await LoadModuleAsync();
        return await module.InvokeAsync<string>(
            "insertChunk",
            await GetHandleAsync(module),
            fileId,
            offset,
            data.Length,
            data);
    }

    public async Task DeleteFileChunksAsync(string fileId)
    {
        IJSInProcessObjectReference module = await LoadModuleAsync();
        await module.InvokeVoidAsync(
            "deleteChunks",
            await GetHandleAsync(module),
            fileId);
    }

    public async Task DeleteFileChunksAsync()
    {
        IJSInProcessObjectReference module = await LoadModuleAsync();
        await module.InvokeVoidAsync(
            "deleteAllChunks",
            await GetHandleAsync(module));
    }

    public async ValueTask DisposeAsync()
    {
        if (m_handle != null)
        {
            IJSInProcessObjectReference module = await LoadModuleAsync();
            module.InvokeVoid("close", m_handle);
            await m_handle.DisposeAsync();
            m_handle = null;
        }

        if (m_module != null)
        {
            await m_module.DisposeAsync();
            m_module = null;
        }

        m_isDisposed = true;
    }

    class Blob : IBlob
    {
        IJSInProcessObjectReference? m_handle;

        public IJSInProcessObjectReference Handle
        {
            get =>
                m_handle ??
                throw new ObjectDisposedException(nameof(Blob));
        }

        public Blob(IJSInProcessObjectReference handle)
        {
            m_handle = handle;
        }

        public void Dispose()
        {
            m_handle?.Dispose();
            m_handle = null;
        }
    }
}
