using System;
using System.Linq;
using Microsoft.AspNetCore.Components.Forms;
using web_p2p_share.Shared.Services.Transfer;

namespace web_p2p_share.Services.Transfer;

public record FileInput(
    string Id,
    int RefCount);

public class FileBrowserService
{
    FileInput[] m_inputs;

    public Action? InputsChanged;
    Action<IFileHandle[]>? FileSelected;
    public Action<string>? OpenBrowserAction;

    public FileInput[] Inputs
    {
        get => m_inputs;
        set
        {
            m_inputs = value;
            InputsChanged?.Invoke();
        }
    }

    public FileBrowserService()
    {
        m_inputs = Array.Empty<FileInput>();
        CreateNewFileInput();
    }

    void CreateNewFileInput()
    {
        string newId = "serverinputfile-" + Guid.NewGuid();
        newId = newId.Replace("-", "");
        Inputs = Inputs
            .Append(new FileInput(newId, 0))
            .ToArray();
    }

    public void OpenBrowser(Action<IFileHandle[]> fileSelectedHandler)
    {
        FileSelected = fileSelectedHandler;
        OpenBrowserAction?.Invoke(Inputs.Last().Id);
    }

    public void HandleFileSelected(IBrowserFile[] files)
    {
        if (files.Length == 0)
        {
            // Is this even possible?
            return;
        }

        // Update inputs ref count.
        FileInput lastInput = Inputs.Last();
        Inputs = Inputs
            .Select(x =>
            {
                if (x.Id == lastInput.Id)
                {
                    // Add ref count.
                    return x with
                    {
                        RefCount = x.RefCount + files.Length
                    };
                }

                return x;
            })
            .ToArray();
        
        // Append new input.
        CreateNewFileInput();

        // Notify file selected handler.
        FileHandle[] handles = files
            .Select(x =>
            {
                FileHandle handle = new FileHandle(lastInput.Id, x);
                handle.Disposed += HandleFileHandleDispose;
                return handle;
            })
            .ToArray();
        FileSelected?.Invoke(handles);
    }

    void HandleFileHandleDispose(FileHandle handle)
    {
        // Unhook and decrease ref count.
        handle.Disposed -= HandleFileHandleDispose;
        Inputs = Inputs
            .Select(x =>
            {
                if (x.Id == handle.InputId)
                {
                    return x with
                    {
                        RefCount = x.RefCount - 1
                    };
                }

                return x;
            })
            .ToArray();
        
        // Clean inputs with no refs except last.
        FileInput last = Inputs.Last();
        Inputs = Inputs
            .Where(x =>
                x.Id == last.Id ||
                x.RefCount > 0)
            .ToArray();
    }
}
