using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using web_p2p_share.Shared.Services.Rtc;

namespace web_p2p_share.Services.Rtc;

public class RtcService : IRtcService, IDisposable
{
    bool m_isDisposed;
    IJSInProcessRuntime m_js;
    IJSInProcessObjectReference? m_module;

    public RtcService(IJSRuntime js)
    {
        m_js = (IJSInProcessRuntime)js;
    }

    async ValueTask<IJSInProcessObjectReference> LoadModuleAsync()
    {
        if (m_isDisposed)
        {
            throw new ObjectDisposedException(nameof(RtcService));
        }

        if (m_module == null)
        {
            m_module = await m_js.InvokeAsync<IJSInProcessObjectReference>(
                "import",
                "./js/Rtc.js");
        }

        return m_module;
    }

    public async Task<IRtcConnection> StartConnectionAsync()
    {
        IJSInProcessObjectReference module = await LoadModuleAsync();
        IJSInProcessObjectReference handle =
            await module.InvokeAsync<IJSInProcessObjectReference>(
                "startConnection");
        return new RtcConnection(module, handle);
    }

    public async Task SetConnectionAnswerAsync(IRtcConnection connection, string remoteSdp)
    {
        await ((RtcConnection)connection)
            .SetConnectionAnswerAsync(remoteSdp);
    }

    public async Task<IRtcConnection> ReceiveConnectionAsync(string remoteSdp)
    {
        IJSInProcessObjectReference module = await LoadModuleAsync();
        IJSInProcessObjectReference handle =
            await module.InvokeAsync<IJSInProcessObjectReference>(
                "receiveConnection",
                remoteSdp);
        return new RtcConnection(module, handle);
    }

    public async ValueTask<byte[]> ReadAsync(IRtcConnection connectionArg, bool isCommand, int count)
    {
        RtcConnection connection = (RtcConnection)connectionArg;
        byte[] result = new byte[count];
        int remaining = count;

        while (remaining > 0)
        {
            if (!connection.IsOpen)
            {
                throw new ObjectDisposedException(nameof(RtcConnection));
            }
            
            byte[] buffer = await connection.ReadAsync(
                isCommand,
                remaining);
            Array.Copy(
                buffer,
                0,
                result,
                count - remaining,
                buffer.Length);
            remaining -= buffer.Length;
        }

        return result;
    }

    public ValueTask WriteAsync(IRtcConnection connectionArg, bool isCommand, Memory<byte> data)
    {
        RtcConnection connection = (RtcConnection)connectionArg;
        connection.Write(isCommand, data.ToArray());
        return ValueTask.CompletedTask;
    }

    public void Dispose()
    {
        if (m_module != null)
        {
            m_module.Dispose();
            m_module = null;
        }

        m_isDisposed = true;
    }

    class RtcConnection : IRtcConnection
    {
        IJSInProcessObjectReference m_module;
        IJSInProcessObjectReference? m_handle;
        LinkedList<byte[]> m_commandBuffer;
        LinkedList<byte[]> m_dataBuffer;
        DotNetObjectReference<RtcConnection>? m_ref;
        TaskCompletionSource? m_readCmdTcs;
        TaskCompletionSource? m_readDataTcs;

        public bool IsOpen
        {
            get
            {
                if (m_handle == null)
                {
                    return false;
                }

                return m_module.Invoke<bool>(
                    "isChannelOpen",
                    m_handle);
            }
        }

        public string Sdp
        {
            get
            {
                if (m_handle == null)
                {
                    throw new ObjectDisposedException(nameof(RtcConnection));
                }

                return m_module.Invoke<string>(
                    "getRemoteSdp",
                    m_handle);
            }
        }

        public RtcConnection(
            IJSInProcessObjectReference module,
            IJSInProcessObjectReference handle)
        {
            m_module = module;
            m_handle = handle;
            m_commandBuffer = new LinkedList<byte[]>();
            m_dataBuffer = new LinkedList<byte[]>();
            m_ref = DotNetObjectReference.Create(this);
            m_module.InvokeVoid("setDotnetRef", m_handle, m_ref);
        }

        [JSInvokable]
        public void PushBuffer(bool isCommand, byte[] data)
        {
            if (m_handle == null ||
                m_ref == null ||
                data.Length == 0)
            {
                return;
            }

            LinkedList<byte[]> bufferList = isCommand ?
                m_commandBuffer : m_dataBuffer;
            bufferList.AddFirst(data);

            if (isCommand)
            {
                m_readCmdTcs?.TrySetResult();
            }
            else
            {
                m_readDataTcs?.TrySetResult();
            }
        }

        [JSInvokable]
        public void HandleChannelClose()
        {
            if (m_handle == null ||
                m_ref == null)
            {
                return;
            }

            Dispose();
        }

        public async Task SetConnectionAnswerAsync(string answer)
        {
            if (m_handle == null)
            {
                throw new ObjectDisposedException(nameof(RtcConnection));
            }

            await m_module.InvokeVoidAsync(
                "setConnectionAnswer",
                m_handle,
                answer);
        }

        public async ValueTask<byte[]> ReadAsync(bool isCommand, int count)
        {
            if (m_handle == null ||
                m_ref == null)
            {
                throw new ObjectDisposedException(nameof(RtcConnection));
            }

            LinkedList<byte[]> bufferList = isCommand ?
                m_commandBuffer : m_dataBuffer;

            if (isCommand)
            {
                if (m_readCmdTcs != null &&
                    !m_readCmdTcs.Task.IsCompleted)
                {
                    await m_readCmdTcs.Task;
                }
            }
            else
            {
                if (m_readDataTcs != null &&
                    !m_readDataTcs.Task.IsCompleted)
                {
                    await m_readDataTcs.Task;
                }
            }

            byte[]? buffer = bufferList.Last?.Value;
            
            if (buffer == null)
            {
                // No buffer yet.
                if (isCommand)
                {
                    m_readCmdTcs = new TaskCompletionSource();
                    await m_readCmdTcs.Task;
                }
                else
                {
                    m_readDataTcs = new TaskCompletionSource();
                    await m_readDataTcs.Task;
                }
            }

            buffer = bufferList.Last?.Value;

            if (buffer == null)
            {
                throw new Exception("Invalid read buffer state");
            }
            
            bufferList.RemoveLast();

            if (buffer.Length <= count)
            {
                // Exact.
                return buffer;
            }
            else
            {
                // Return excess buffer.
                byte[] excess = new byte[buffer.Length - count];
                buffer.AsMemory()
                    .Slice(count)
                    .CopyTo(excess);
                bufferList.AddLast(excess);

                // Resize result.
                byte[] result = new byte[count];
                buffer.AsMemory()
                    .Slice(0, count)
                    .CopyTo(result);
                return result;
            }
        }

        public void Write(bool isCommand, byte[] data)
        {
            m_module.InvokeVoid(
                "writeToChannel",
                m_handle,
                data,
                isCommand);
        }

        public void Dispose()
        {
            if (m_handle != null)
            {
                m_module.InvokeVoid("closeConnection", m_handle);
                m_handle.Dispose();
                m_handle = null;
            }

            if (m_ref != null)
            {
                m_ref.Dispose();
                m_ref = null;
            }

            m_commandBuffer.Clear();
            m_dataBuffer.Clear();
            m_readCmdTcs?.TrySetCanceled();
            m_readDataTcs?.TrySetCanceled();
        }
    }
}
