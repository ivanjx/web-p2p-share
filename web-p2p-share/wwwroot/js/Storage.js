const DB_NAME = "tempdb";
const STORAGE_NAME = "tempFileChunks";

/**
 * Creates and opens indexeddb database.
 * @throws `db_not_supported` if indexeddb is not supported in the browser
 * @throws `db_open_fail` errors while opening the database
 * @throws `db_create_fail` errors when trying to create the database
 * @returns {Promise<IDBDatabase>} An indexeddb database object.
 */
export async function open() {
    // Check if indexed db is supported or not.
    if (!window.indexedDB) {
        throw new Error("db_not_supported");
    }

    // Opening database.
    const openDbRequest = indexedDB.open(DB_NAME, 1);
    return await new Promise(function(resolve, reject) {
        openDbRequest.onerror = function(e) {
            console.error(openDbRequest.error);
            reject(new Error("db_open_fail"));
        };
        openDbRequest.onsuccess = function(e) {
            // Database already exists.
            resolve(openDbRequest.result);
        };
        openDbRequest.onupgradeneeded = function(e) {
            // Creating database.
            const objectStore = openDbRequest.result.createObjectStore(
                STORAGE_NAME,
                {
                    keyPath: "id",
                    autoIncrement: true
                }
            );
            
            try {
                objectStore.createIndex(
                    "fileId",
                    "fileId", // Quick chunks querying of a single file.
                    {
                        unique: false
                    }
                );
                objectStore.createIndex(
                    "offset",
                    "offset", // Faster access if offsets are sorted?
                    {
                        unique: false
                    }
                );
            } catch (ex) {
                console.error(ex);
                reject(new Error("db_create_fail"));
            }

            objectStore.transaction.oncomplete = function(e) {
                // Done.
                resolve(openDbRequest.result);
            };
        };
    });
}

/**
 * 
 * @param {IDBDatabase} db 
 */
export function close(db) {
    db.close();
}

/**
 * Inserts a new chunk of the file into the database.
 * @param {IDBDatabase} db 
 * @param {string} fileId 
 * @param {number} offset 
 * @param {number} size 
 * @param {Uint8Array} data 
 * @returns {Promise}
 */
export function insertChunk(db, fileId, offset, size, data) {
    const chunksStore = db
        .transaction(STORAGE_NAME, "readwrite")
        .objectStore(STORAGE_NAME);
    const dataBlob = new Blob([data], { type: "application/octet-stream" });
    const result = chunksStore.add({
        fileId: fileId,
        offset: offset,
        size: size,
        data: dataBlob
    });

    return new Promise(function(resolve, reject) {
        result.onerror = function (e) {
            reject(result.error);
        };
        result.onsuccess = function (e) {
            resolve();
        };
    });
}

/**
 * Deletes all the chunks.
 * @param {IDBDatabase} db The database object from `open`
 * @see {@link open}
 * @returns {Promise}
 */
export function deleteAllChunks(db) {
    const chunksStore = db
        .transaction(STORAGE_NAME, "readwrite")
        .objectStore(STORAGE_NAME);
    const result = chunksStore.clear();
    
    return new Promise(function(resolve, reject) {
        result.onerror = function(e) {
            reject(e);
        };
        result.onsuccess = function(e) {
            resolve();
        }
    });
}

/**
 * Deletes the chunks of a given file id.
 * @param {IDBDatabase} db The database object from `open`
 * @see {@link open}
 * @param {string} fileId The id of the file
 * @returns {Promise}
 */
export function deleteChunks(db, fileId) {
    const chunksStore = db
        .transaction(STORAGE_NAME, "readwrite")
        .objectStore(STORAGE_NAME);
    const cursor = chunksStore.index("fileId").openKeyCursor(fileId);
    
    return new Promise(function(resolve, reject) {
        cursor.onerror = function(e) {
            reject(cursor.error);
        };
        cursor.onsuccess = function(e) {
            const current = cursor.result;

            if (current) {
                chunksStore.delete(current.primaryKey);
                current.continue();
            } else {
                resolve();
            }
        };
    });
}

/**
 * 
 * @param {IDBDatabase} db 
 * @param {string} fileId 
 * @returns {Promise<Blob>}
 */
export function getFileBlob(db, fileId) {
    const chunksStore = db
        .transaction(STORAGE_NAME, "readonly")
        .objectStore(STORAGE_NAME);
    const cursor = chunksStore.index("fileId").openKeyCursor(fileId);
    /**
     * @type {Blob[]}
     */
    const blobs = [];

    return new Promise(function(resolve, reject) {
        cursor.onerror = function(e) {
            reject(cursor.error);
        };
        cursor.onsuccess = function(e) {
            const current = cursor.result;

            if (current) {
                const readRequest = chunksStore.get(current.primaryKey);
                readRequest.onerror = function (ee) {
                    reject(ee);
                };
                readRequest.onsuccess = function(ee) {
                    blobs.push(readRequest.result.data);
                    current.continue();
                };
            } else {
                resolve(new Blob(blobs, { type: "application/octet-stream" }));
            }
        };
    });
}

/**
 * 
 * @param {Blob} blob 
 * @param {string} name 
 */
export function downloadFileBlob(blob, name) {
    const link = document.getElementById("dlink");
    link.href = URL.createObjectURL(blob);
    link.download = name;
    link.click();
}
