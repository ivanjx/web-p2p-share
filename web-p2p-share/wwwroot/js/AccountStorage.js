const DB_NAME = "accountdb"
const STORAGE_NAME = "account";

/**
 * @throws `db_not_supported`
 * @throws `db_open_fail`
 * @throws `db_create_fail`
 * @returns {Promise<IDBDatabase>}
 */
export async function open() {
    // Check for indexed db support.
    if (!window.indexedDB) {
        throw new Error("db_not_supported");
    }

    // Opening database.
    const openDbRequest = indexedDB.open(DB_NAME, 1);
    return await new Promise(function(resolve, reject) {
        openDbRequest.onerror = function(e) {
            console.error(openDbRequest.error);
            reject(new Error("db_open_fail"));
        };
        openDbRequest.onsuccess = function(e) {
            // Database already exists.
            resolve(openDbRequest.result);
        };
        openDbRequest.onupgradeneeded = function(e) {
            // Creating database.
            const objectStore = openDbRequest.result.createObjectStore(
                STORAGE_NAME,
                {
                    keyPath: "id",
                    autoIncrement: true
                }
            );

            try {
                objectStore.createIndex(
                    "id",
                    "id",
                    {
                        unique: true
                    }
                );
                objectStore.createIndex(
                    "username",
                    "username",
                    {
                        unique: false
                    }
                );
            } catch (ex) {
                console.error(ex);
                reject(new Error("db_create_fail"));
            }

            objectStore.transaction.oncomplete = function(e) {
                // Done.
                resolve(openDbRequest.result);
            };
        };
    });
}

/**
 * 
 * @param {IDBDatabase} db 
 */
export function close(db) {
    db.close();
}

/**
 * @typedef {Object} User
 * @property {string} id
 * @property {string} username
 */

/**
 * @param {IDBDatabase} db
 * @returns {Promise<User>}
 */
export function get(db) {
    const accountStore = db
        .transaction(STORAGE_NAME, "readonly")
        .objectStore(STORAGE_NAME);
    const request = accountStore.getAll();

    return new Promise(function(resolve, reject) {
        request.onerror = function (e) {
            reject(request.error);
        };
        request.onsuccess = function(e) {
            if (request.result.length == 0) {
                resolve(null);
            } else {
                resolve(request.result[0]);
            }
        };
    });
}

/**
 * @param {IDBDatabase} db
 * @param {User} user
 * @returns {Promise}
 */
export async function set(db, user) {
    const currentUser = await get(db);
    const accountStore = db
        .transaction(STORAGE_NAME, "readwrite")
        .objectStore(STORAGE_NAME);

    let userId = user.id;

    if (currentUser != null) {
        userId = currentUser.id;
    }

    const request = accountStore.put({
        id: userId,
        username: user.username
    });

    return await new Promise(function(resolve, reject) {
        request.onerror = function(e) {
            reject(request.error);
        };
        request.onsuccess = function(e) {
            resolve();
        };
    });
}
