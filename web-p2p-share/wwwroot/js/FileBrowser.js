/**
 * 
 * @param {string} id 
 */
export function openBrowser(id) {
    const element = document.getElementById(id);
    element.click();
}
