/**
 * Checks whether the browser supports webcam operation.
 * @returns {boolean} `true` if the browser supports webcam
 */
export function check() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        return true;
    } else {
        return false;
    }
}

/**
 * @typedef {Object} StartResult
 * @property {HTMLElement} video The video element
 * @property {MediaStream} stream The camera stream
 * @property {CanvasRenderingContext2D} context The 2d context of the hidden canvas
 */

/**
 * Starts the webcam. Initializes the hidden canvas object for image retrieval from the webcam.
 * @throws `webcam_not_granted` if the user did not allow the webcam access
 * @param {string} videoId The id of the video element
 * @param {string} canvasId The id of the canvas element
 * @returns {StartResult}
 */
export async function start(videoId, canvasId) {
    const video = document.getElementById(videoId);
    let stream;
    
    try {
        stream = await navigator.mediaDevices.getUserMedia({
            audio: false,
            video: {
                width: { max: 500, ideal: 500 },
                height: { max: 500, ideal: 500 },
                facingMode: "environment"
            }
        });
    } catch (err) {
        if (err.name == "NotAllowedError") {
            throw new Error("webcam_not_granted");
        } else {
            throw err;
        }
    }

    if ("srcObject" in video) {
        video.srcObject = stream;
    } else {
        video.src = URL.createObjectURL(stream);
    }

    video.onloadedmetadata = function() { video.play(); };

    return {
        video: video,
        stream: stream,
        context: document.getElementById(canvasId).getContext("2d", { willReadFrequently: true })
    }
}

/**
 * Gets the image pixel data from the webcam via the hidden canvas.
 * @param {StartResult} result The result of start.
 * @see {@link start}
 * @returns {Uint8Array} The image pixel data in byte array format.
 */
export function getImage(result) {
    result.context.drawImage(result.video, 0, 0);
    const data = result.context.getImageData(0, 0, 500, 500);
    return new Uint8Array(data.data);
}

/**
 * 
 * @param {StartResult} result 
 */
export function stop(result) {
    result.stream.getTracks().forEach(
        track => track.stop()
    );
}
