/**
 * @typedef {Object} RtcHandle
 * @property {RTCPeerConnection} connection
 * @property {RTCDataChannel} commandChannel
 * @property {RTCDataChannel} dataChannel
 * @property {string} sdp
 * @property {any} dotnetRef
 */

/**
 * 
 * @returns {Promise<RtcHandle>}
 */
export async function startConnection() {
    const connection = new RTCPeerConnection({
        iceServers: []
    });
    const commandChannel = connection.createDataChannel(
        "command",
        { ordered: true });
    const dataChannel = connection.createDataChannel(
        "data",
        { ordered: true });
    const offer = await connection.createOffer();
    await connection.setLocalDescription(offer);

    let handle = {
        connection: connection,
        commandChannel: commandChannel,
        dataChannel: dataChannel,
        sdp: null,
        dotnetRef: null
    };

    return await new Promise(function(resolve, reject) {
        connection.onicecandidateerror = function(e) {
            reject(e.errorText);
        };
        connection.onicecandidate = function(e) {
            if (connection.iceGatheringState == "complete") {
                handle.sdp = connection.localDescription.sdp;
                resolve(handle);
            }
        };
        connection.ondatachannel = function(e) {
            const isCommand = e.channel.label == "command";
            e.channel.onmessage = function(e) {
                const data = new Uint8Array(e.data);
                handle.dotnetRef?.invokeMethod(
                    "PushBuffer",
                    isCommand,
                    data
                );
            };
            e.channel.onclose = function() {
                handle.dotnetRef?.invokeMethod(
                    "HandleChannelClose");
            };
        }
    });
}

/**
 * 
 * @param {RtcHandle} handle 
 * @param {string} remoteSdp 
 */
export async function setConnectionAnswer(handle, remoteSdp) {
    await handle.connection.setRemoteDescription(
        new RTCSessionDescription({
            type: "answer",
            sdp: remoteSdp
        })
    );
}

/**
 * 
 * @param {string} remoteSdp 
 * @returns {Promise<RtcHandle>}
 */
export async function receiveConnection(remoteSdp) {
    const connection = new RTCPeerConnection({
        iceServers: []
    });
    const commandChannel = connection.createDataChannel(
        "command",
        { ordered: true });
    const dataChannel = connection.createDataChannel(
        "data",
        { ordered: true });
    await connection.setRemoteDescription(
        new RTCSessionDescription({
            type: "offer",
            sdp: remoteSdp
        })
    );
    const answer = await connection.createAnswer();
    await connection.setLocalDescription(answer);
    
    let handle = {
        connection: connection,
        commandChannel: commandChannel,
        dataChannel: dataChannel,
        sdp: null,
        dotnetRef: null
    };

    return await new Promise(function(resolve, reject) {
        connection.onicecandidateerror = function(e) {
            reject(e.errorText);
        };
        connection.onicecandidate = function(e) {
            if (connection.iceGatheringState == "complete") {
                handle.sdp = connection.localDescription.sdp;
                resolve(handle);
            }
        };
        connection.ondatachannel = function(e) {
            const isCommand = e.channel.label == "command";
            e.channel.onmessage = function(e) {
                const data = new Uint8Array(e.data);
                handle.dotnetRef?.invokeMethod(
                    "PushBuffer",
                    isCommand,
                    data
                );
            };
            e.channel.onclose = function() {
                handle.dotnetRef?.invokeMethod(
                    "HandleChannelClose");
            };
        }
    });
}

/**
 * 
 * @param {RtcHandle} handle 
 * @param {any} dotnetRef 
 */
export function setDotnetRef(handle, dotnetRef) {
    handle.dotnetRef = dotnetRef;
}

/**
 * 
 * @param {RtcHandle} handle 
 * @returns {string}
 */
export function getRemoteSdp(handle) {
    return handle.sdp;
}

/**
 * 
 * @param {RtcHandle} handle 
 * @returns {boolean}
 */
export function isChannelOpen(handle) {
    const command = handle.commandChannel.readyState == "open";
    const data = handle.dataChannel.readyState == "open";
    return command && data;
}

/**
 * 
 * @param {RtcHandle} handle 
 */
export function closeConnection(handle) {
    handle.connection.close();
}

/**
 * 
 * @param {RtcHandle} handle 
 * @param {Uint8Array} data 
 * @param {boolean} isCommand 
 */
export function writeToChannel(handle, data, isCommand) {
    if (isCommand) {
        handle.commandChannel.send(data);
    } else {
        handle.dataChannel.send(data);
    }
}
