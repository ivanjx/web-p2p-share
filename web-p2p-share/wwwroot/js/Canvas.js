/**
 * 
 * @typedef {Object} Result The initialization state
 * @property {HTMLElement} result.canvas The canvas element
 * @property {CanvasRenderingContext2D} result.context The canvas element's 2d context
 */

/**
 * 
 * @param {string} id The id of the canvas element
 * @returns {Result}
 */
export function init(id) {
    const canvas = document.getElementById(id);
    return {
        canvas: canvas,
        context: canvas.getContext("2d")
    };
}

/**
 * 
 * @param {Result} initResult
 * @returns {Number}
 */
export function getWidth(initResult) {
    return initResult.canvas.clientWidth;
}

/**
 * 
 * @param {Result} initResult
 * @returns {Number}
 */
export function getHeight(initResult) {
    return initResult.canvas.clientHeight;
}

/**
 * Draws image pixel data into the canvas
 * @param {Result} initResult The result from init. @see {@link init}
 * @param {Uint8Array} data Image pixel data in the form of byte array.
 */
export function draw(initResult, data) {
    const width = initResult.canvas.width = initResult.canvas.clientWidth;
    const height = initResult.canvas.height = initResult.canvas.clientHeight;
    const imageData = initResult.context.createImageData(width, height);
    imageData.data.set(
        Uint8ClampedArray.from(data)
    );
    initResult.context.putImageData(
        imageData,
        0,
        0);
}
