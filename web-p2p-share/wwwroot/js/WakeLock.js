/**
 * 
 * @returns {Promise}
 */
async function lock() {
    if (document.visibilityState != "visible") {
        return;
    }

    try {
        lock = await navigator.wakeLock.request("screen");
    } catch (e) {
        throw new Error("Cannot acquire lock: " + e.message);
    }
}

/**
 * 
 * @returns {Promise}
 */
export async function startLock() {
    const supported = "wakeLock" in navigator;

    if (!supported) {
        throw new Error("Wake lock not supported");
    }

    lock();
    document.addEventListener(
        "visibilitychange",
        lock
    );
}
