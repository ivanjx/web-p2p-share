using System;
using System.Linq;
using Microsoft.AspNetCore.Components;
using web_p2p_share.Shared.Services.Transfer;

namespace web_p2p_share.Main;

public partial class ClientTransfersView : IDisposable
{
    public TransferViewModel[] Transfers
    {
        get;
        set;
    }

    [Parameter]
    public string SenderId
    {
        get;
        set;
    }

    [Inject]
    public ClientState ClientState
    {
        get;
        set;
    }

    [Inject]
    public IClientService ClientService
    {
        get;
        set;
    }

    public ClientTransfersView()
    {
        Transfers = new TransferViewModel[0];
        SenderId = "";
        ClientState = null!;
        ClientService = null!;
    }

    protected override void OnInitialized()
    {
        ClientState.FileTransfersChanged += HandleTransfersChange;
        HandleTransfersChange();
    }

    void HandleTransfersChange()
    {
        Transfers = ClientState.FileTransfers
            .Where(x => x.SenderId == SenderId)
            .Select(x => new TransferViewModel(x))
            .ToArray();
        StateHasChanged();

        foreach (TransferViewModel vm in Transfers)
        {
            vm.StopRequested += HandleTransferStopRequest;
            vm.SaveRequested += HandleTransferSaveRequest;
        }
    }

    void HandleTransferStopRequest(ClientFileTransfer transfer)
    {
        ClientService.StopDownload(
            transfer.SenderId,
            transfer.File.Id);
    }

    void HandleTransferSaveRequest(ClientFileTransfer transfer)
    {
        _ = ClientService.SaveAsync(
            transfer.SenderId,
            transfer.File.Id);
    }

    public void Dispose()
    {
        ClientState.FileTransfersChanged -= HandleTransfersChange;
    }

    public class TransferViewModel
    {
        ClientFileTransfer m_transfer;

        public Action<ClientFileTransfer>? StopRequested;
        public Action<ClientFileTransfer>? SaveRequested;

        public string Name
        {
            get => m_transfer.File.Name;
        }

        public bool IsTransferring
        {
            get =>
                m_transfer.ReceivedSize < m_transfer.File.Size &&
                m_transfer.ReceivedSize > 0;
        }

        public bool IsStopped
        {
            get => IsTransferring && m_transfer.CancellationTokenSource == null;
        }

        public bool IsCompleted
        {
            get => m_transfer.ReceivedSize == m_transfer.File.Size;
        }

        public string Status
        {
            get
            {
                if (IsTransferring)
                {
                    if (IsStopped)
                    {
                        return "Stopped";
                    }
                    else
                    {
                        return string.Format(
                            "{0:F2} of {1:F2} MB",
                            m_transfer.ReceivedSize / (1024D * 1024D),
                            m_transfer.File.Size / (1024D * 1024D));
                    }
                }
                else
                {
                    if (IsCompleted)
                    {
                        return "Completed";
                    }
                    else
                    {
                        return "Queued";
                    }
                }
            }
        }

        public TransferViewModel(ClientFileTransfer transfer)
        {
            m_transfer = transfer;
        }

        public void Stop()
        {
            StopRequested?.Invoke(m_transfer);
        }

        public void Save()
        {
            SaveRequested?.Invoke(m_transfer);
        }
    }
}
