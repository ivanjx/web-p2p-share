using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using web_p2p_share.Shared.Services.Rtc;
using web_p2p_share.Shared.Services.Transfer;

namespace web_p2p_share.Main;

public partial class ClientView : IDisposable
{
    const int TRANSFERS_ANIMATION_DURATION = 300;

    public string Username
    {
        get;
        set;
    }

    public FileViewModel[] Files
    {
        get;
        set;
    }

    public bool IsTransfersOpen
    {
        get;
        set;
    }

    public bool IsTransfersAnimating
    {
        get;
        set;
    }

    public string? TransfersCssClass
    {
        get;
        set;
    }

    public string? TransfersOverlayCssClass
    {
        get;
        set;
    }

    [Parameter]
    public string SenderId
    {
        get;
        set;
    }

    [Inject]
    public ConnectionState ConnectionState
    {
        get;
        set;
    }

    [Inject]
    public ClientState ClientState
    {
        get;
        set;
    }

    [Inject]
    public IClientService ClientService
    {
        get;
        set;
    }

    [Inject]
    public NavigationManager NavigationManager
    {
        get;
        set;
    }

    public ClientView()
    {
        Username = "";
        Files = new FileViewModel[0];
        SenderId = "";
        ConnectionState = null!;
        ClientState = null!;
        ClientService = null!;
        NavigationManager = null!;
    }

    protected override void OnInitialized()
    {
        Username = ConnectionState.Senders
            .Where(x => x.Request.Peer.Id == SenderId)
            .FirstOrDefault()?
            .Request.Peer.Username ?? "-";
        ClientState.FileListsChanged += HandleFileListsChange;
        HandleFileListsChange();
    }

    void HandleFileListsChange()
    {
        Files = ClientState.FileLists
            .Where(x => x.SenderId == SenderId)
            .FirstOrDefault()?.Files
            .Select(x => new FileViewModel(x))
            .ToArray() ??
            new FileViewModel[0];
        
        foreach (FileViewModel vm in Files)
        {
            vm.DownloadRequested += HandleFileDownloadRequest;
        }

        StateHasChanged();
    }

    void HandleFileDownloadRequest(ClientFileInfo file)
    {
        ClientService.StartDownload(SenderId, file.Id);
    }

    public void GoBack()
    {
        NavigationManager.GoBack();
    }

    public async Task LoadFilesAsync()
    {
        try
        {
            await ClientService.LoadFilesAsync(SenderId);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error loading files {0}: {1}", SenderId, ex);
        }
    }

    public async Task ToggleTransfersAsync()
    {
        if (IsTransfersAnimating)
        {
            return;
        }

        IsTransfersAnimating = true;

        if (IsTransfersOpen)
        {
            // Animate closing.
            TransfersCssClass = "visible slide-out";
            await Task.Delay(TRANSFERS_ANIMATION_DURATION);

            // Remove overlay & menu.
            TransfersOverlayCssClass = null;
            TransfersCssClass = null;

            // Done.
            IsTransfersOpen = false;
        }
        else
        {
            // Show overlay.
            TransfersOverlayCssClass = "visible";

            // Animate opening.
            TransfersCssClass = "visible slide-in";
            await Task.Delay(TRANSFERS_ANIMATION_DURATION);

            // Done.
            IsTransfersOpen = true;
        }

        IsTransfersAnimating = false;
    }

    public void Dispose()
    {
        ClientState.FileListsChanged -= HandleFileListsChange;
    }

    public class FileViewModel
    {
        ClientFileInfo m_file;

        public Action<ClientFileInfo>? DownloadRequested;

        public string Name
        {
            get => m_file.Name;
        }

        public string SizeStr
        {
            get => (m_file.Size / (1024D * 1024D)).ToString("F2") + " MB";
        }

        public FileViewModel(ClientFileInfo file)
        {
            m_file = file;
        }

        public void Download()
        {
            DownloadRequested?.Invoke(m_file);
        }
    }
}
