using System;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using web_p2p_share.Shared.Services.Rtc;

namespace web_p2p_share.Main;

public partial class ReceiverDiscoveryView : IDisposable
{
    public ReceiverViewModel[] Receivers
    {
        get;
        set;
    }

    [Inject]
    public ConnectionState ConnectionState
    {
        get;
        set;
    }

    [Inject]
    public NavigationManager NavigationManager
    {
        get;
        set;
    }

    [Inject]
    public IDiscoveryTaskService DiscoveryTaskService
    {
        get;
        set;
    }

    [Inject]
    public IConnectionService ConnectionService
    {
        get;
        set;
    }

    public ReceiverDiscoveryView()
    {
        Receivers = new ReceiverViewModel[0];

        ConnectionState = null!;
        NavigationManager = null!;
        DiscoveryTaskService = null!;
        ConnectionService = null!;
    }

    protected override void OnInitialized()
    {
        ConnectionState.ReceiversChanged += HandleReceiversChange;
        DiscoveryTaskService.StartSender();
        HandleReceiversChange();
    }

    void HandleReceiversChange()
    {
        foreach (ReceiverViewModel vm in Receivers)
        {
            vm.PropertyChanged -= HandleReceiverPropertyChange;
            vm.Dispose();
        }

        Receivers = ConnectionState.Receivers
            .Select(x =>
            {
                ReceiverViewModel vm = new ReceiverViewModel(
                    ConnectionState,
                    ConnectionService,
                    NavigationManager,
                    x.Receiver);
                vm.PropertyChanged += HandleReceiverPropertyChange;
                return vm;
            })
            .ToArray();
        StateHasChanged();
    }

    void HandleReceiverPropertyChange()
    {
        StateHasChanged();
    }

    public void GoBack()
    {
        NavigationManager.GoBack();
    }

    public void OpenQr()
    {
        NavigationManager.NavigateTo("/receiver-discovery/qr");
    }

    public void Dispose()
    {
        ConnectionState.ReceiversChanged -= HandleReceiversChange;
        DiscoveryTaskService.Stop();

        foreach (ReceiverViewModel vm in Receivers)
        {
            vm.PropertyChanged -= HandleReceiverPropertyChange;
            vm.Dispose();
        }

        Receivers = new ReceiverViewModel[0];
    }

    public class ReceiverViewModel : IDisposable
    {
        ConnectionState m_connectionState;
        IConnectionService m_connectionService;
        NavigationManager m_navigationManager;

        RtcPeer m_receiver;
        bool m_isConnecting;
        bool m_isConnected;

        public Action? PropertyChanged;

        public string Username
        {
            get => m_receiver.Username;
        }

        public bool IsConnecting
        {
            get => m_isConnecting;
            set
            {
                m_isConnecting = value;
                PropertyChanged?.Invoke();
            }
        }

        public bool IsConnected
        {
            get => m_isConnected;
            set
            {
                m_isConnected = value;
                PropertyChanged?.Invoke();
            }
        }

        public ReceiverViewModel(
            ConnectionState connectionState,
            IConnectionService connectionService,
            NavigationManager navigationManager,
            RtcPeer receiver)
        {
            m_connectionState = connectionState;
            m_connectionService = connectionService;
            m_navigationManager = navigationManager;
            m_receiver = receiver;

            m_connectionState.ReceiversChanged += HandleReceiversChange;
            HandleReceiversChange();
        }

        void HandleReceiversChange()
        {
            ReceiverConnection? connection = m_connectionState.Receivers
                .Where(x => x.Receiver.Id == m_receiver.Id)
                .FirstOrDefault();
            
            if (connection == null)
            {
                return; // Impossible.
            }

            IsConnecting = connection.CreateConnectionCancellationTokenSource != null;
            IsConnected = connection.Connection?.IsOpen ?? false;
            PropertyChanged?.Invoke();
        }

        public void Connect()
        {
            if (IsConnecting)
            {
                return;
            }

            if (IsConnected)
            {
                m_navigationManager.NavigateTo("/server/" + m_receiver.Id);
            }
            else
            {
                m_connectionService.ConnectToReceiver(m_receiver);
            }
        }

        public void CancelConnect()
        {
            if (!IsConnecting)
            {
                return;
            }

            m_connectionService.CancelConnectToReceiver(m_receiver);
        }

        public void Dispose()
        {
            m_connectionState.ReceiversChanged -= HandleReceiversChange;
        }
    }
}
