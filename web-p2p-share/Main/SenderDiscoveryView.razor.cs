using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using web_p2p_share.Shared.Services.Rtc;

namespace web_p2p_share.Main;

public partial class SenderDiscoveryView : IDisposable
{
    public RequestViewModel[] Requests
    {
        get;
        set;
    }

    [Inject]
    public ConnectionState ConnectionState
    {
        get;
        set;
    }

    [Inject]
    public NavigationManager NavigationManager
    {
        get;
        set;
    }

    [Inject]
    public IDiscoveryTaskService DiscoveryTaskService
    {
        get;
        set;
    }

    [Inject]
    public IConnectionService ConnectionService
    {
        get;
        set;
    }

    public SenderDiscoveryView()
    {
        Requests = new RequestViewModel[0];
        ConnectionState = null!;
        NavigationManager = null!;
        DiscoveryTaskService = null!;
        ConnectionService = null!;
    }

    protected override void OnInitialized()
    {
        ConnectionState.SendersChanged += HandleSendersChange;
        DiscoveryTaskService.StartReceiver();
        HandleSendersChange();
    }

    void HandleSendersChange()
    {
        foreach (RequestViewModel vm in Requests)
        {
            vm.PropertyChanged -= HandleRequestPropertyChange;
            vm.Dispose();
        }

        Requests = ConnectionState.Senders
            .Select(x =>
            {
                RequestViewModel vm = new RequestViewModel(
                    ConnectionState,
                    ConnectionService,
                    NavigationManager,
                    x.Request.Peer);
                vm.PropertyChanged += HandleRequestPropertyChange;
                return vm;
            })
            .ToArray();
        StateHasChanged();
    }

    void HandleRequestPropertyChange()
    {
        StateHasChanged();
    }

    public void GoBack()
    {
        NavigationManager.GoBack();
    }

    public void ScanQr()
    {
        NavigationManager.NavigateTo("/sender-discovery/qr");
    }

    public void Dispose()
    {
        ConnectionState.SendersChanged -= HandleSendersChange;
        DiscoveryTaskService.Stop();

        foreach (RequestViewModel vm in Requests)
        {
            vm.PropertyChanged -= HandleRequestPropertyChange;
            vm.Dispose();
        }

        Requests = new RequestViewModel[0];
    }

    public class RequestViewModel : IDisposable
    {
        ConnectionState m_connectionState;
        IConnectionService m_connectionService;
        NavigationManager m_navigationManager;

        RtcPeer m_sender;
        bool m_isConnecting;
        bool m_isConnected;

        public Action? PropertyChanged;

        public string Username
        {
            get => m_sender.Username;
        }

        public bool IsAnswering
        {
            get => m_isConnecting;
            set
            {
                m_isConnecting = value;
                PropertyChanged?.Invoke();
            }
        }

        public bool IsAnswered
        {
            get => m_isConnected;
            set
            {
                m_isConnected = value;
                PropertyChanged?.Invoke();
            }
        }

        public RequestViewModel(
            ConnectionState connectionState,
            IConnectionService connectionService,
            NavigationManager navigationManager,
            RtcPeer sender)
        {
            m_connectionState = connectionState;
            m_connectionService = connectionService;
            m_navigationManager = navigationManager;
            m_sender = sender;

            m_connectionState.SendersChanged += HandleSendersChange;
            HandleSendersChange();
        }

        void HandleSendersChange()
        {
            SenderConnection? connection = m_connectionState.Senders
                .Where(x => x.Request.Peer.Id == m_sender.Id)
                .FirstOrDefault();
            
            if (connection == null)
            {
                return; // Impossible.
            }

            IsAnswering = connection.AnswerConnectionCancellationTokenSource != null;
            IsAnswered = connection.Connection?.IsOpen ?? false;
            PropertyChanged?.Invoke();
        }

        public void Answer()
        {
            if (IsAnswering)
            {
                return;
            }

            if (IsAnswered)
            {
                m_navigationManager.NavigateTo("/client/" + m_sender.Id);
            }
            else
            {
                m_connectionService.ReceiveConnectionRequest(m_sender);
            }
        }

        public void CancelAnswer()
        {
            if (!IsAnswering)
            {
                return;
            }

            m_connectionService.CancelAnswerToSender(m_sender);
        }

        public void Dispose()
        {
            m_connectionState.SendersChanged -= HandleSendersChange;
        }
    }
}
