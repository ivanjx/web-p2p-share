using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;

namespace web_p2p_share.Main;

public partial class MainView
{
    const int MENU_ANIMATION_DURATION = 300;

    public bool IsMenuOpen
    {
        get;
        set;
    }

    public bool IsMenuAnimating
    {
        get;
        set;
    }

    public string? MenuCssClass
    {
        get;
        set;
    }

    public string? MenuOverlayCssClass
    {
        get;
        set;
    }

    [Inject]
    public NavigationManager NavigationManager
    {
        get;
        set;
    }

    public MainView()
    {
        NavigationManager = null!;
    }

    public void OpenReceiverDiscovery()
    {
        NavigationManager.NavigateTo("/receiver-discovery");
    }

    public void OpenSenderDiscovery()
    {
        NavigationManager.NavigateTo("/sender-discovery");
    }

    public async Task ToggleMenuAsync()
    {
        if (IsMenuAnimating)
        {
            return;
        }

        IsMenuAnimating = true;

        if (IsMenuOpen)
        {
            // Animate closing.
            MenuCssClass = "visible slide-out";
            await Task.Delay(MENU_ANIMATION_DURATION);

            // Remove overlay & menu.
            MenuOverlayCssClass = null;
            MenuCssClass = null;

            // Done.
            IsMenuOpen = false;
        }
        else
        {
            // Show overlay.
            MenuOverlayCssClass = "visible";

            // Animate opening.
            MenuCssClass = "visible slide-in";
            await Task.Delay(MENU_ANIMATION_DURATION);

            // Done.
            IsMenuOpen = true;
        }

        IsMenuAnimating = false;
    }
}
