using System;
using System.Linq;
using Microsoft.AspNetCore.Components;
using web_p2p_share.Services.Transfer;
using web_p2p_share.Shared.Services.Rtc;
using web_p2p_share.Shared.Services.Transfer;

namespace web_p2p_share.Main;

public partial class ServerView : IDisposable
{
    public string Username
    {
        get;
        set;
    }

    public FileViewModel[] Files
    {
        get;
        set;
    }

    [Parameter]
    public string ReceiverId
    {
        get;
        set;
    }

    [Inject]
    public ServerState ServerState
    {
        get;
        set;
    }

    [Inject]
    public ConnectionState ConnectionState
    {
        get;
        set;
    }

    [Inject]
    public FileBrowserService FileBrowserService
    {
        get;
        set;
    }

    [Inject]
    public IServerService ServerService
    {
        get;
        set;
    }

    [Inject]
    public NavigationManager NavigationManager
    {
        get;
        set;
    }

    public ServerView()
    {
        Username = "";
        Files = new FileViewModel[0];
        ReceiverId = "";
        ServerState = null!;
        ConnectionState = null!;
        FileBrowserService = null!;
        ServerService = null!;
        NavigationManager = null!;
    }

    protected override void OnInitialized()
    {
        Username = ConnectionState.Receivers
            .Where(x => x.Receiver.Id == ReceiverId)
            .FirstOrDefault()?
            .Receiver.Username ?? "-";
        ServerState.FileListsChanged += HandleFileListsChange;
        HandleFileListsChange();
    }

    void HandleFileListsChange()
    {
        Files = ServerState.FileLists
            .Where(x => x.ReceiverId == ReceiverId)
            .FirstOrDefault()?
            .Files
            .Select(x => new FileViewModel(x))
            .ToArray() ??
            new FileViewModel[0];
        
        foreach (FileViewModel vm in Files)
        {
            vm.DeleteRequested += HandleFileDeleteRequest;
        }

        StateHasChanged();
    }

    void HandleFileDeleteRequest(ServerFileInfo file)
    {
        ServerService.RemoveFile(ReceiverId, file.Id);
    }

    public void GoBack()
    {
        NavigationManager.GoBack();
    }

    public void Browse()
    {
        FileBrowserService.OpenBrowser(HandleAddFiles);
    }

    void HandleAddFiles(IFileHandle[] files)
    {
        ServerService.AddFiles(
            ReceiverId,
            files);
    }

    public void Dispose()
    {
        ServerState.FileListsChanged -= HandleFileListsChange;
    }

    public class FileViewModel
    {
        ServerFileInfo m_file;

        public Action<ServerFileInfo>? DeleteRequested;

        public string Name
        {
            get => m_file.Name;
        }

        public string SizeStr
        {
            get => (m_file.Size / (1024D * 1024D)).ToString("F2") + " MB";
        }

        public FileViewModel(ServerFileInfo file)
        {
            m_file = file;
        }

        public void Delete()
        {
            DeleteRequested?.Invoke(m_file);
        }
    }
}
