using System;
using System.Linq;
using System.Runtime.InteropServices.JavaScript;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;
using web_p2p_share.Services.Transfer;

namespace web_p2p_share.Main;

public partial class FileBrowser : IDisposable
{
    string[] m_inputIds;

    [Inject]
    public FileBrowserService FileBrowserService
    {
        get;
        set;
    }

    public FileBrowser()
    {
        m_inputIds = new string[0];
        FileBrowserService = null!;
    }

    protected override async Task OnInitializedAsync()
    {
        if (!OperatingSystem.IsBrowser())
        {
            throw new PlatformNotSupportedException();
        }
        
        await JSHost.ImportAsync(
            "FileBrowser",
            "/js/FileBrowser.js");
        FileBrowserService.OpenBrowserAction = OpenBrowser;
        FileBrowserService.InputsChanged += HandleInputsChange;

        await Task.Delay(1);
        HandleInputsChange();
    }

    void HandleInputsChange()
    {
        m_inputIds = FileBrowserService.Inputs
            .Select(x => x.Id)
            .ToArray();
        StateHasChanged();
    }

    void HandleInputFileChange(InputFileChangeEventArgs e)
    {
        FileBrowserService.HandleFileSelected(
            e.GetMultipleFiles(1000).ToArray());
    }

    public void Dispose()
    {
        FileBrowserService.OpenBrowserAction = null;
        FileBrowserService.InputsChanged -= HandleInputsChange;
    }

    [JSImport("openBrowser", "FileBrowser")]
    public static partial void OpenBrowser(string id);
}
