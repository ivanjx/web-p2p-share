using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using web_p2p_share.Shared.Services.Media;
using web_p2p_share.Shared.Services.Rtc;

namespace web_p2p_share.Main;

public partial class SenderDiscoveryQrView : IDisposable
{
    public bool IsScanningOffer
    {
        get;
        set;
    }

    public string ScannerVideoId
    {
        get;
        set;
    }

    public string ScannerCanvasId
    {
        get;
        set;
    }

    public string QrCanvasId
    {
        get;
        set;
    }

    [Inject]
    public QrConnectionState QrConnectionState
    {
        get;
        set;
    }

    [Inject]
    public IQrConnectionService QrConnectionService
    {
        get;
        set;
    }

    [Inject]
    public IQrService QrService
    {
        get;
        set;
    }

    [Inject]
    public ICanvasService CanvasService
    {
        get;
        set;
    }

    [Inject]
    public IWebCamService WebCamService
    {
        get;
        set;
    }

    [Inject]
    public NavigationManager NavigationManager
    {
        get;
        set;
    }

    public SenderDiscoveryQrView()
    {
        IsScanningOffer = true;
        ScannerVideoId = "scvid" + Guid.NewGuid().ToString().Replace("-", "");
        ScannerCanvasId = "sccvid" + Guid.NewGuid().ToString().Replace("-", "");
        QrCanvasId = "qrcvid" + Guid.NewGuid().ToString().Replace("-", "");

        QrConnectionState = null!;
        QrConnectionService = null!;
        QrService = null!;
        CanvasService = null!;
        WebCamService = null!;
        NavigationManager = null!;
    }

    protected override void OnAfterRender(bool firstRender)
    {
        if (!firstRender)
        {
            return; 
        }

        WaitOffer();
    }

    async void WaitOffer()
    {
        _ = QrConnectionService.WaitOfferAsync();
        IWebCamHandle scannerCanvas;

        try
        {
            Console.WriteLine("Initializing offer scanner canvas");
            scannerCanvas = await WebCamService.StartAsync(
                ScannerVideoId,
                ScannerCanvasId);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Unable to initialize offer scanner canvas: {0}", ex);
            GoBack();
            return;
        }

        while (true)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));

            if (QrConnectionState.Connection == null)
            {
                Console.WriteLine("Cancelled");
                WebCamService.Stop(scannerCanvas);
                scannerCanvas.Dispose();
                GoBack();
                return;
            }

            if (QrConnectionState.Answer != null)
            {
                Console.WriteLine("Stopping offer scanner");
                WebCamService.Stop(scannerCanvas);
                scannerCanvas.Dispose();
                break;
            }

            if (QrConnectionState.Offer != null)
            {
                // Still scanning previous answer.
                continue;
            }

            // Getting new qr data.
            byte[] dataBuff = WebCamService.GetImage(scannerCanvas);
            QrConnectionState.Offer = await Task.Run(() =>
                QrService.Decode(dataBuff, 500, 500));
        }

        Console.WriteLine("Showing answer");
        IsScanningOffer = false;
        StateHasChanged();
        await Task.Delay(1); // Delay a bit to render elements.

        Console.WriteLine("Drawing answer qr");
        using ICanvasHandle qrCanvas = CanvasService.Init(QrCanvasId);
        int cvHeight = (int)CanvasService.GetHeight(qrCanvas);
        int cvWidth = (int)CanvasService.GetWidth(qrCanvas);
        byte[] qrData = await Task.Run(() =>
            QrService.Encode(
                QrConnectionState.Answer,
                cvWidth,
                cvHeight));
        CanvasService.Draw(
            qrCanvas,
            qrData);
        
        while (true)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));

            if (QrConnectionState.Connection == null)
            {
                // Either canceled or connected.
                Console.WriteLine("Qr discovery done");
                break;
            }
        }

        GoBack();
    }

    public void GoBack()
    {
        if (NavigationManager.Uri.EndsWith("/qr"))
        {
            NavigationManager.GoBack();
        }
    }

    public void Dispose()
    {
        QrConnectionService.Cancel();
    }
}
