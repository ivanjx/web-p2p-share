using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using web_p2p_share.Shared.Services.Account;

namespace web_p2p_share.Main;

public partial class SettingsView : IAsyncDisposable
{
    [Inject]
    public AccountState AccountState
    {
        get;
        set;
    }

    [Inject]
    public IAccountService AccountService
    {
        get;
        set;
    }

    public string Username
    {
        get;
        set;
    }

    public bool IsEditorOpen
    {
        get;
        set;
    }

    public string NewUsername
    {
        get;
        set;
    }

    public SettingsView()
    {
        AccountState = null!;
        AccountService = null!;
        Username = "";
        NewUsername = "";
    }

    protected override void OnInitialized()
    {
        AccountState.AccountChanged += UpdateState;
        UpdateState();
    }

    void UpdateState()
    {
        Username = AccountState.Username;
        StateHasChanged();
    }

    public void OpenEditor()
    {
        IsEditorOpen = true;
        NewUsername = AccountState.Username;
    }

    public void CloseEditor()
    {
        IsEditorOpen = false;
    }

    public async Task SaveAsync()
    {
        await AccountService.ChangeUsernameAsync(NewUsername);
        IsEditorOpen = false;
    }

    public ValueTask DisposeAsync()
    {
        AccountState.AccountChanged -= UpdateState;
        return ValueTask.CompletedTask;
    }
}
