using System;
using System.Runtime.InteropServices.JavaScript;
using Microsoft.AspNetCore.Components;

namespace web_p2p_share.Main;

public static partial class Extensions
{
    [JSImport("globalThis.history.back")]
    public static partial void _GoBack();

    public static void GoBack(this NavigationManager nav)
    {
        _GoBack();
    }
}
