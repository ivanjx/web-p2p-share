using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using web_p2p_share.Shared.Services.Media;
using web_p2p_share.Shared.Services.Rtc;

namespace web_p2p_share.Main;

public partial class ReceiverDiscoveryQrView : IDisposable
{
    public bool IsShowingOffer
    {
        get;
        set;
    }

    public string QrCanvasId
    {
        get;
        set;
    }

    public string ScannerVideoId
    {
        get;
        set;
    }

    public string ScannerCanvasId
    {
        get;
        set;
    }

    [Inject]
    public QrConnectionState QrConnectionState
    {
        get;
        set;
    }

    [Inject]
    public IQrConnectionService QrConnectionService
    {
        get;
        set;
    }

    [Inject]
    public IQrService QrService
    {
        get;
        set;
    }

    [Inject]
    public ICanvasService CanvasService
    {
        get;
        set;
    }

    [Inject]
    public IWebCamService WebCamService
    {
        get;
        set;
    }

    [Inject]
    public NavigationManager NavigationManager
    {
        get;
        set;
    }

    public ReceiverDiscoveryQrView()
    {
        IsShowingOffer = true;
        QrCanvasId = "qrcvid" + Guid.NewGuid().ToString().Replace("-", "");
        ScannerVideoId = "scvid" + Guid.NewGuid().ToString().Replace("-", "");
        ScannerCanvasId = "sccvid" + Guid.NewGuid().ToString().Replace("-", "");

        QrConnectionState = null!;
        QrConnectionService = null!;
        QrService = null!;
        CanvasService = null!;
        WebCamService = null!;
        NavigationManager = null!;
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        if (!firstRender)
        {
            return;
        }

        Console.WriteLine("Initializing offer");
        await QrConnectionService.CreateOfferAsync();

        if (QrConnectionState.Offer == null)
        {
            Console.WriteLine("Unable to generate offer");
            GoBack();
            return;
        }

        Console.WriteLine("Drawing offer qr");
        using ICanvasHandle handle = CanvasService.Init(QrCanvasId);
        int cvHeight = (int)CanvasService.GetHeight(handle);
        int cvWidth = (int)CanvasService.GetWidth(handle);
        byte[] qrData = await Task.Run(() =>
            QrService.Encode(
                QrConnectionState.Offer,
                cvWidth,
                cvHeight));
        CanvasService.Draw(
            handle,
            qrData);
    }

    public void GoBack()
    {
        if (NavigationManager.Uri.EndsWith("/qr"))
        {
            NavigationManager.GoBack();
        }
    }

    public async void WaitAnswer()
    {
        Console.WriteLine("Showing answer scanner");
        IsShowingOffer = false;
        await Task.Delay(1); // Give time to render the elements.

        _ = QrConnectionService.WaitAnswerAsync();        
        IWebCamHandle handle;

        try
        {
            Console.WriteLine("Initializing answer scanner canvas");
            handle = await WebCamService.StartAsync(
                ScannerVideoId,
                ScannerCanvasId);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Unable to initializing answer scanner canvas: {0}", ex);
            GoBack();
            return;
        }

        while (true)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));

            if (QrConnectionState.Connection == null)
            {
                Console.WriteLine("Stopping answer scanner");
                WebCamService.Stop(handle);
                handle.Dispose();
                break;
            }

            if (QrConnectionState.Answer != null)
            {
                // Still scanning previous answer.
                continue;
            }

            // Getting new qr data.
            byte[] dataBuff = WebCamService.GetImage(handle);
            QrConnectionState.Answer = await Task.Run(() =>
                QrService.Decode(dataBuff, 500, 500));
        }

        GoBack();
    }

    public void Dispose()
    {
        QrConnectionService.Cancel();
    }
}
