using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Net.Http;
using web_p2p_share;
using web_p2p_share.Services.Account;
using web_p2p_share.Services.Media;
using web_p2p_share.Services.Rtc;
using web_p2p_share.Services.Transfer;
using web_p2p_share.Shared.Services.Account;
using web_p2p_share.Shared.Services.Media;
using web_p2p_share.Shared.Services.Rtc;
using web_p2p_share.Shared.Services.Transfer;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");

builder.Services
    // States.
    .AddSingleton<AccountState>()
    .AddSingleton<ConnectionState>()
    .AddSingleton<QrConnectionState>()
    .AddSingleton<ServerState>()
    .AddSingleton<ClientState>()

    // Services.
    .AddSingleton<FileBrowserService>()
    .AddScoped(_ => new HttpClient
    {
        BaseAddress = new Uri(
            builder.HostEnvironment.BaseAddress)
    })
    .AddScoped<IQrService, QrService>()
    .AddScoped<IAccountStorageService, AccountStorageService>()
    .AddScoped<IAccountService, AccountService>()
    .AddScoped<ICanvasService, CanvasService>()
    .AddScoped<IWebCamService, WebCamService>()
    .AddScoped<IWakeLockService, WakeLockService>()
    .AddScoped<IRtcService, RtcService>()
    .AddScoped<ISdpService, SdpService>()
    .AddScoped<ISignallingService, SignallingService>()
    .AddScoped<IDiscoveryTaskService, DiscoveryTaskService>()
    .AddScoped<IConnectionService, ConnectionService>()
    .AddScoped<IQrConnectionService, QrConnectionService>()
    .AddScoped<IStorageService, StorageService>()
    .AddScoped<IServerService, ServerService>()
    .AddScoped<IServerRunnerService, ServerRunnerService>()
    .AddScoped<IClientService, ClientService>()
    .AddScoped<IClientRunnerService, ClientRunnerService>();

WebAssemblyHost host = builder.Build();
await CanvasService.InitAsync();
await WebCamService.InitAsync();
await WakeLockService.InitAsync();
await host.Services.GetService<IAccountService>()!.InitAsync();
_ = host.Services.GetService<IStorageService>()!.DeleteFileChunksAsync();
host.Services.GetService<IServerRunnerService>()!.Start();
host.Services.GetService<IClientRunnerService>()!.Start();
host.Services.GetService<IWakeLockService>()!.AcquireLock();
await host.RunAsync();
