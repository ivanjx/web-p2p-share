using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog;
using web_p2p_share.Signalling.Controllers;

namespace web_p2p_share.Signalling.Middleware;

public class ExceptionHandlerMiddleware
{
    RequestDelegate m_next;

    public ExceptionHandlerMiddleware(RequestDelegate next)
    {
        m_next = next;
    }

    public async Task Invoke(HttpContext context, ILogger logger)
    {
        try
        {
            await m_next.Invoke(context);
        }
        catch (Exception ex)
        {
            logger.Error(ex, ex.Message);

            // Override response.
            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            await context.Response.WriteAsJsonAsync(
                new ErrorResult()
                {
                    Error = ex.GetType().Name,
                    Details = ex.Message,
                    TraceId = context.TraceIdentifier
                },
                ControllersJsonSerializerContext.Default.ErrorResult);
        }
    }
}
