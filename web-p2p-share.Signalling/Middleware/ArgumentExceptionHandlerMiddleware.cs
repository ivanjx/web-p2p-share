using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Serilog;
using web_p2p_share.Signalling.Controllers;

namespace web_p2p_share.Signalling.Middleware;

public class ArgumentExceptionHandlerMiddleware
{
    RequestDelegate m_next;

    public ArgumentExceptionHandlerMiddleware(RequestDelegate next)
    {
        m_next = next;
    }

    public async Task Invoke(HttpContext context, ILogger logger)
    {
        try
        {
            await m_next.Invoke(context);
        }
        catch (ArgumentException ex)
        {
            logger.Error(ex, ex.Message);

            // Override response.
            context.Response.StatusCode = StatusCodes.Status400BadRequest;
            await context.Response.WriteAsJsonAsync(
                new ErrorResult()
                {
                    Error = ex.GetType().Name,
                    Details = ex.Message,
                    TraceId = context.TraceIdentifier
                },
                ControllersJsonSerializerContext.Default.ErrorResult);
        }
    }
}
