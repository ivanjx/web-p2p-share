using System;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace web_p2p_share.Signalling.Controllers;

public class IndexController
{
    public static void Map(WebApplication app)
    {
        IndexController controller = new IndexController();
        app.MapGet("/", controller.GetIndexAsync);
    }

    public Task<IndexResult> GetIndexAsync()
    {
        return Task.FromResult(new IndexResult());
    }

    public record IndexResult
    {
        [JsonPropertyName("message")]
        public string Message
        {
            get;
            set;
        }

        public IndexResult()
        {
            Message = "hello world!";
        }
    }
}
