using System;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using web_p2p_share.Signalling.Services;

namespace web_p2p_share.Signalling.Controllers;

public class ClientController
{
    public static void Map(WebApplication app)
    {
        ClientController controller = new ClientController(
            app.Services.GetRequiredService<IRequestIpAccessorService>(),
            app.Services.GetRequiredService<IClientService>());

        RouteGroupBuilder group = app.MapGroup("/client");
        group.MapGet("/list", controller.ListAsync);
        group.MapPost("/add", controller.AddAsync);
    }

    IRequestIpAccessorService m_requestIpAccessorService;
    IClientService m_clientService;

    public ClientController(
        IRequestIpAccessorService requestIpAccessorService,
        IClientService clientService)
    {
        m_requestIpAccessorService = requestIpAccessorService;
        m_clientService = clientService;
    }

    public async Task<ClientResponse[]> ListAsync()
    {
        string ip = m_requestIpAccessorService.Get();
        Client[] clients = await m_clientService.ListAsync(ip);
        ClientResponse[] response = clients
            .Select(x => new ClientResponse()
            {
                Id = x.Id,
                Username = x.Username
            })
            .ToArray();

        return response;
    }

    public async Task AddAsync(AddRequest? request)
    {
        if (request == null ||
            string.IsNullOrEmpty(request.Id) ||
            string.IsNullOrEmpty(request.Username))
        {
            throw new ArgumentException("Invalid request");
        }

        string ip = m_requestIpAccessorService.Get();
        await m_clientService.AddAsync(
            request.Id,
            request.Username,
            ip);
    }

    public record ClientResponse
    {
        [JsonPropertyName("id")]
        public string Id
        {
            get;
            set;
        }

        [JsonPropertyName("username")]
        public string Username
        {
            get;
            set;
        }

        public ClientResponse()
        {
            Id = "";
            Username = "";
        }
    }

    public record AddRequest
    {
        [JsonPropertyName("id")]
        public string? Id
        {
            get;
            set;
        }

        [JsonPropertyName("username")]
        public string? Username
        {
            get;
            set;
        }
    }
}
