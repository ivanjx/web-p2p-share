using System;
using System.Text.Json.Serialization;

namespace web_p2p_share.Signalling.Controllers;

[JsonSerializable(typeof(IndexController.IndexResult))]

[JsonSerializable(typeof(ClientConnectionController.CreateRequest))]
[JsonSerializable(typeof(ClientConnectionController.GetAnswerResponse))]
[JsonSerializable(typeof(ClientConnectionController.DeleteRequest))]
[JsonSerializable(typeof(ClientConnectionController.GetRequestsResponse))]
[JsonSerializable(typeof(ClientConnectionController.GetRequestsResponse[]))]
[JsonSerializable(typeof(ClientConnectionController.SetAnswerRequest))]

[JsonSerializable(typeof(ClientController.ClientResponse))]
[JsonSerializable(typeof(ClientController.ClientResponse[]))]
[JsonSerializable(typeof(ClientController.AddRequest))]

[JsonSerializable(typeof(ErrorResult))]
public partial class ControllersJsonSerializerContext : JsonSerializerContext
{
}
