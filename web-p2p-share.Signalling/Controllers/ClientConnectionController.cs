using System;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using web_p2p_share.Signalling.Services;

namespace web_p2p_share.Signalling.Controllers;

public class ClientConnectionController
{
    public static void Map(WebApplication app)
    {
        ClientConnectionController controller = new ClientConnectionController(
            app.Services.GetRequiredService<IClientConnectionService>());

        RouteGroupBuilder group = app.MapGroup("/connection");
        group.MapPost("/create", controller.CreateAsync);
        group.MapPost("/delete", controller.DeleteAsync);
        group.MapGet("/get-requests", controller.GetRequestsAsync);
        group.MapGet("/get-answer", controller.GetAnswerAsync);
        group.MapPost("/answer", controller.SetAnswerAsync);
    }

    IClientConnectionService m_connectionService;

    public ClientConnectionController(
        IClientConnectionService connectionService)
    {
        m_connectionService = connectionService;
    }

    public async Task CreateAsync(CreateRequest? request)
    {
        if (request == null ||
            string.IsNullOrEmpty(request.Id) ||
            string.IsNullOrEmpty(request.ToId) ||
            string.IsNullOrEmpty(request.Sdp))
        {
            throw new ArgumentException("Invalid request");
        }

        await m_connectionService.CreateConnectionRequestAsync(
            request.Id,
            request.ToId,
            request.Sdp);
    }

    public async Task DeleteAsync(DeleteRequest? request)
    {
        if (request == null ||
            string.IsNullOrEmpty(request.Id) ||
            string.IsNullOrEmpty(request.ToId))
        {
            throw new ArgumentException("Invalid request");
        }

        await m_connectionService.DeleteConnectionRequestAsync(
            request.Id,
            request.ToId);
    }

    public async Task<GetRequestsResponse[]> GetRequestsAsync(
        [FromQuery(Name = "id")] string? id)
    {
        if (string.IsNullOrEmpty(id))
        {
            throw new ArgumentException("Invalid request");
        }

        ConnectionRequest[] requests = await m_connectionService.GetConnectionRequestsAsync(id);
        return requests
            .Select(x => new GetRequestsResponse()
            {
                FromId = x.From.Id,
                FromUsername = x.From.Username,
                Sdp = x.Sdp
            })
            .ToArray();
    }

    public async Task<GetAnswerResponse> GetAnswerAsync(
        [FromQuery(Name = "id")] string? id,
        [FromQuery(Name = "toId")] string? toId)
    {
        if (string.IsNullOrEmpty(id) ||
            string.IsNullOrEmpty(toId))
        {
            throw new ArgumentException("Invalid request");
        }

        string? result = await m_connectionService.GetAnswerSdpAsync(id, toId);
        return new GetAnswerResponse()
        {
            Answer = result
        };
    }

    public async Task SetAnswerAsync(SetAnswerRequest? request)
    {
        if (request == null ||
            string.IsNullOrEmpty(request.Id) ||
            string.IsNullOrEmpty(request.FromId) ||
            string.IsNullOrEmpty(request.Sdp))
        {
            throw new ArgumentException("Invalid request");
        }

        await m_connectionService.AnswerConnectionRequestAsync(
            request.FromId,
            request.Id,
            request.Sdp);
    }

    public record CreateRequest
    {
        [JsonPropertyName("id")]
        public string? Id
        {
            get;
            set;
        }

        [JsonPropertyName("toId")]
        public string? ToId
        {
            get;
            set;
        }

        [JsonPropertyName("sdp")]
        public string? Sdp
        {
            get;
            set;
        }
    }

    public record GetAnswerResponse
    {
        [JsonPropertyName("answer")]
        public string? Answer
        {
            get;
            set;
        }
    }

    public record DeleteRequest
    {
        [JsonPropertyName("id")]
        public string? Id
        {
            get;
            set;
        }

        [JsonPropertyName("toId")]
        public string? ToId
        {
            get;
            set;
        }
    }

    public record GetRequestsResponse
    {
        [JsonPropertyName("fromId")]
        public string FromId
        {
            get;
            set;
        }

        [JsonPropertyName("fromUsername")]
        public string FromUsername
        {
            get;
            set;
        }

        [JsonPropertyName("sdp")]
        public string Sdp
        {
            get;
            set;
        }

        public GetRequestsResponse()
        {
            FromId = "";
            FromUsername = "";
            Sdp = "";
        }
    }

    public record SetAnswerRequest
    {
        [JsonPropertyName("id")]
        public string? Id
        {
            get;
            set;
        }

        [JsonPropertyName("fromId")]
        public string? FromId
        {
            get;
            set;
        }

        [JsonPropertyName("sdp")]
        public string? Sdp
        {
            get;
            set;
        }
    }
}
