using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace web_p2p_share.Signalling.Controllers;

public record ErrorResult
{
    [JsonPropertyName("error")]
    public string Error
    {
        get;
        init;
    }

    [JsonPropertyName("details")]
    public string Details
    {
        get;
        init;
    }

    [JsonPropertyName("traceId")]
    public string TraceId
    {
        get;
        init;
    }

    public ErrorResult()
    {
        Error = "";
        Details = "";
        TraceId = "";
    }
}
