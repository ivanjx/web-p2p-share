using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using web_p2p_share.Signalling.Services;

namespace web_p2p_share.Signalling.Jobs;

public class ClientCleanerJob : BackgroundService
{
    const int INTERVAL_SECS = 1;

    IServiceProvider m_provider;

    public ClientCleanerJob(
        IServiceProvider provider)
    {
        m_provider = provider;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        using IServiceScope scope = m_provider.CreateScope();
        ILogger log = scope.ServiceProvider.GetService<ILogger>()!;
        IClientService clientService = scope.ServiceProvider.GetService<IClientService>()!;
        log = log.ForContext<ClientCleanerJob>();
        log.Information("Starting client cleaner job");

        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                await Task.Delay(
                    TimeSpan.FromSeconds(INTERVAL_SECS),
                    stoppingToken);
                await clientService.ClearOldClientsAsync();
            }
            catch (Exception ex)
            {
                log.Error(ex, "Client cleaner job error");
            }
        }
    }
}
