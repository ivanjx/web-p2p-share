using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using web_p2p_share.Signalling.Services;

namespace web_p2p_share.Signalling.Jobs;

public class ClientConnectionCleanerJob : BackgroundService
{
    const int INTERVAL_SECS = 5;

    IServiceProvider m_provider;

    public ClientConnectionCleanerJob(
        IServiceProvider provider)
    {
        m_provider = provider;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        using IServiceScope scope = m_provider.CreateScope();
        ILogger log = scope.ServiceProvider.GetService<ILogger>()!;
        IClientConnectionService connectionService =
            scope.ServiceProvider.GetService<IClientConnectionService>()!;
        log = log.ForContext<ClientConnectionCleanerJob>();
        log.Information("Starting client connection cleaner job");

        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                await Task.Delay(
                    TimeSpan.FromSeconds(INTERVAL_SECS),
                    stoppingToken);
                await connectionService.DeleteOldConnectionsAsync();
            }
            catch (Exception ex)
            {
                log.Error(ex, "Client connection cleaner job error");
            }
        }
    }
}
