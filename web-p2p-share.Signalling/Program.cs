using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Serilog.Events;
using web_p2p_share.Signalling.Controllers;
using web_p2p_share.Signalling.Jobs;
using web_p2p_share.Signalling.Middleware;
using web_p2p_share.Signalling.Services;

// App config.
ConfigurationService configurationService = new ConfigurationService();

// Serilog.
var loggerConfiguration = new LoggerConfiguration()
    .MinimumLevel.Override("Microsoft", LogEventLevel.Error)
    .MinimumLevel.Override("System", LogEventLevel.Error)
    .Enrich.FromLogContext();

if (configurationService.SeqEndpoint != null &&
    configurationService.SeqApiKey != null)
{
    // Use seq.
    loggerConfiguration = loggerConfiguration
        .WriteTo.Seq(
            configurationService.SeqEndpoint,
            apiKey: configurationService.SeqApiKey);
}
else
{
    // Use console.
    loggerConfiguration = loggerConfiguration
        .WriteTo.Console();
}

Log.Logger = loggerConfiguration.CreateLogger();

try
{
    WebApplicationBuilder builder = WebApplication.CreateSlimBuilder(args);
    builder.WebHost.UseUrls(configurationService.ListenEndpoint);
    builder.Host.UseSerilog();

    builder.Services
        // Controllers json serializer context.
        .ConfigureHttpJsonOptions(options =>
        {
            options.SerializerOptions.TypeInfoResolverChain.Insert(
                0,
                ControllersJsonSerializerContext.Default);
        })

        // Register built-in services.
        .AddCors()
        .AddHttpClient()
        .AddHttpContextAccessor()
        .AddSerilog(Log.Logger)

        // Register custom services.
        .AddSingleton<IRequestIpAccessorService, RequestIpAccessorService>()
        .AddSingleton<IConfigurationService>(configurationService)
        .AddSingleton<IClientRepository, ClientRepository>()
        .AddSingleton<IClientConnectionRepository, ClientConnectionRepository>()
        .AddSingleton<ITimeService, TimeService>()
        .AddSingleton<IClientGroupService, ClientGroupService>()
        .AddSingleton<IClientService, ClientService>()
        .AddSingleton<IClientConnectionService, ClientConnectionService>()

        // Register jobs.
        .AddHostedService<ClientCleanerJob>()
        .AddHostedService<ClientConnectionCleanerJob>();
    
    WebApplication application = builder.Build();
    application
        // All starts with /api.
        .UsePathBase(configurationService.BasePath)

        // Request logging.
        .UseSerilogRequestLogging()

        // Exception handlers (from bottom -> top).
        .UseMiddleware<ExceptionHandlerMiddleware>()
        .UseMiddleware<ArgumentExceptionHandlerMiddleware>()

        // CORS.
        .UseCors(builder => builder
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials()
            .SetIsOriginAllowed(origin =>
                origin.StartsWith("https://localhost") || // Allow from localhost.
                origin.Contains(".trycloudflare.com"))) // Allow from cloudflared tunnel.

        // Routing.
        .UseRouting()

        // Websocket.
        .UseWebSockets(new WebSocketOptions()
        {
            KeepAliveInterval = TimeSpan.FromMinutes(1)
        });

    // Controllers.
    IndexController.Map(application);
    ClientController.Map(application);
    ClientConnectionController.Map(application);

    // Done.
    await application.RunAsync();
}
catch (Exception ex)
{
    Log.Fatal(ex, "Web server error");
}
finally
{
    Log.CloseAndFlush();
}
