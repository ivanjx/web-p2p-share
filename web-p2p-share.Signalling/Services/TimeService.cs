using System;

namespace web_p2p_share.Signalling.Services;

public interface ITimeService
{
    DateTime GetCurrentTime();
}

public class TimeService : ITimeService
{
    public DateTime GetCurrentTime()
    {
        return DateTime.UtcNow;
    }
}
