using System;
using System.Linq;

namespace web_p2p_share.Signalling.Services;

public interface IClientGroupService
{
    string GetGroup(string ip);
}

public class ClientGroupService : IClientGroupService
{
    public string GetGroup(string ip)
    {
        if (ip.Contains(":"))
        {
            // IPv6.
            return "v6";
        }

        // IPv4.
        int[] ipParts = ip
            .Split('.')
            .Select(s => int.Parse(s))
            .ToArray();
        
        if (ipParts.Length != 4)
        {
            throw new ArgumentException("Invalid ipv4 address: " + ip);
        }

        bool is10Range = ipParts[0] == 10;
        bool is172Range = ipParts[0] == 172 && (ipParts[1] >= 16 && ipParts[1] <= 31);
        bool is192Range = ipParts[0] == 192 && ipParts[1] == 168;

        if (is10Range || is172Range || is192Range)
        {
            // Private IP.
            return "default";
        }

        // Public ip.
        return ipParts.First().ToString();
    }
}
