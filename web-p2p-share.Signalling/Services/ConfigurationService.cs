using System;

namespace web_p2p_share.Signalling.Services;

public interface IConfigurationService
{
    string? SeqEndpoint
    {
        get;
    }

    string? SeqApiKey
    {
        get;
    }

    string ListenEndpoint
    {
        get;
    }

    string BasePath
    {
        get;
    }
}

public class ConfigurationService : IConfigurationService
{
    public string BasePath => "/api";

    public string ListenEndpoint
    {
        get;
        private set;
    }

    public string? SeqEndpoint
    {
        get;
        private set;
    }

    public string? SeqApiKey
    {
        get;
        private set;
    }

    public ConfigurationService()
    {
        ListenEndpoint =
            Environment.GetEnvironmentVariable("P2PSHARE_LISTEN_ENDPOINT") ??
            throw new Exception("Missing P2PSHARE_LISTEN_ENDPOINT");
        SeqEndpoint = Environment.GetEnvironmentVariable("P2PSHARE_SEQ_ENDPOINT");
        SeqApiKey = Environment.GetEnvironmentVariable("P2PSHARE_SEQ_API_KEY");
    }
}
