using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace web_p2p_share.Signalling.Services;

public record Client(
    string Id,
    string Group,
    string Username,
    DateTime LastSeen);

public interface IClientRepository
{
    Task<Client[]> ListAsync();
    Task<Client[]> ListAsync(string group);
    Task<Client?> GetAsync(string id);
    Task AddAsync(Client client);
    Task UpdateAsync(Client client);
    Task RemoveAsync(string id);
}

public class ClientRepository : IClientRepository
{
    Client[] m_clients;
    SemaphoreSlim m_semaphore;

    public ClientRepository()
    {
        m_clients = new Client[0];
        m_semaphore = new SemaphoreSlim(1);
    }

    public async Task AddAsync(Client client)
    {
        await m_semaphore.WaitAsync();

        try
        {
            Client? duplicate = m_clients
                .Where(x => x.Id == client.Id)
                .FirstOrDefault();

            if (duplicate != null)
            {
                throw new Exception("Client already exists: " + client.Id);
            }

            m_clients = m_clients
                .Append(client)
                .ToArray();
        }
        finally
        {
            m_semaphore.Release();
        }
    }

    public async Task<Client?> GetAsync(string id)
    {
        await m_semaphore.WaitAsync();

        try
        {
            return m_clients
                .Where(x => x.Id == id)
                .FirstOrDefault();
        }
        finally
        {
            m_semaphore.Release();
        }
    }

    public async Task<Client[]> ListAsync(string group)
    {
        await m_semaphore.WaitAsync();

        try
        {
            return m_clients
                .Where(x => x.Group == group)
                .ToArray();
        }
        finally
        {
            m_semaphore.Release();
        }
    }

    public async Task<Client[]> ListAsync()
    {
        await m_semaphore.WaitAsync();

        try
        {
            return m_clients;
        }
        finally
        {
            m_semaphore.Release();
        }
    }

    public async Task RemoveAsync(string id)
    {
        await m_semaphore.WaitAsync();

        try
        {
            m_clients = m_clients
                .Where(x => x.Id != id)
                .ToArray();
        }
        finally
        {
            m_semaphore.Release();
        }
    }

    public async Task UpdateAsync(Client client)
    {
        await m_semaphore.WaitAsync();

        try
        {
            m_clients = m_clients
                .Select(x => x.Id == client.Id ? client : x)
                .ToArray();
        }
        finally
        {
            m_semaphore.Release();
        }
    }
}
