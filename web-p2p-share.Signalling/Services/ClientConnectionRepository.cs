using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace web_p2p_share.Signalling.Services;

public record ClientConnection(
    string FromId,
    string ToId,
    string RequestSdp,
    string? AnswerSdp,
    DateTime CreationTime);

public interface IClientConnectionRepository
{
    Task<ClientConnection[]> ListAsync();
    Task<ClientConnection?> GetAsync(
        string fromId,
        string toId);
    Task AddAsync(ClientConnection connection);
    Task ReplaceAsync(ClientConnection connection);
    Task DeleteAsync(
        string fromId,
        string toId);
}

public class ClientConnectionRepository : IClientConnectionRepository
{
    ClientConnection[] m_connections;
    SemaphoreSlim m_semaphore;

    public ClientConnectionRepository()
    {
        m_connections = new ClientConnection[0];
        m_semaphore = new SemaphoreSlim(1);
    }

    public async Task AddAsync(ClientConnection connection)
    {
        await m_semaphore.WaitAsync();

        try
        {
            ClientConnection? duplicate = m_connections
                .Where(x =>
                    x.FromId == connection.FromId &&
                    x.ToId == connection.ToId)
                .FirstOrDefault();
            
            if (duplicate != null)
            {
                throw new Exception("Connection already exists");
            }

            m_connections = m_connections
                .Append(connection)
                .ToArray();
        }
        finally
        {
            m_semaphore.Release();
        }
    }

    public async Task<ClientConnection?> GetAsync(string fromId, string toId)
    {
        await m_semaphore.WaitAsync();

        try
        {
            return m_connections
                .Where(x =>
                    x.FromId == fromId &&
                    x.ToId == toId)
                .FirstOrDefault();
        }
        finally
        {
            m_semaphore.Release();
        }
    }

    public async Task<ClientConnection[]> ListAsync()
    {
        await m_semaphore.WaitAsync();

        try
        {
            return m_connections;
        }
        finally
        {
            m_semaphore.Release();
        }
    }

    public async Task DeleteAsync(string fromId, string toId)
    {
        await m_semaphore.WaitAsync();

        try
        {
            m_connections = m_connections
                .Where(x =>
                    x.FromId != fromId ||
                    x.ToId != toId)
                .ToArray();
        }
        finally
        {
            m_semaphore.Release();
        }
    }

    public async Task ReplaceAsync(ClientConnection connection)
    {
        await m_semaphore.WaitAsync();

        try
        {
            m_connections = m_connections
                .Select(x =>
                {
                    if (x.FromId == connection.FromId &&
                        x.ToId == connection.ToId)
                    {
                        return connection;
                    }

                    return x;
                })
                .ToArray();
        }
        finally
        {
            m_semaphore.Release();
        }
    }
}
