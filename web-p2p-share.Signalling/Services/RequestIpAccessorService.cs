using System;
using System.Linq;
using Microsoft.AspNetCore.Http;

namespace web_p2p_share.Signalling.Services;

public interface IRequestIpAccessorService
{
    string Get();
}

public class RequestIpAccessorService : IRequestIpAccessorService
{
    IHttpContextAccessor m_contextAccessor;

    public RequestIpAccessorService(
        IHttpContextAccessor contextAccessor)
    {
        m_contextAccessor = contextAccessor;
    }

    public string Get()
    {
        if (m_contextAccessor.HttpContext == null)
        {
            throw new Exception("HttpContext is null");
        }

        // Get from x-forwarded-for.
        string? clientIp = m_contextAccessor.HttpContext.Request.Headers["X-FORWARDED-FOR"]
            .FirstOrDefault();
        
        if (string.IsNullOrEmpty(clientIp))
        {
            // Use default.
            clientIp =
                m_contextAccessor.HttpContext.Connection.RemoteIpAddress?.ToString() ??
                "0.0.0.0"; // Unknown.
        }

        if (clientIp.Contains(','))
        {
            clientIp = clientIp.Split(',').First();
        }

        return clientIp;
    }
}
