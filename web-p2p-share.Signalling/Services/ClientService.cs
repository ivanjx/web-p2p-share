using System;
using System.Threading.Tasks;
using Serilog;

namespace web_p2p_share.Signalling.Services;

public interface IClientService
{
    Task AddAsync(
        string id,
        string username,
        string ip);
    Task<Client[]> ListAsync(string ip);
    Task ClearOldClientsAsync();
}

public class ClientService : IClientService
{
    IClientRepository m_clientRepository;
    IClientGroupService m_groupService;
    ITimeService m_timeService;
    ILogger m_log;

    ILogger Log
    {
        get => m_log.ForContext<ClientService>();
    }

    public ClientService(
        IClientRepository clientRepository,
        IClientGroupService groupService,
        ITimeService timeService,
        ILogger log)
    {
        m_clientRepository = clientRepository;
        m_groupService = groupService;
        m_timeService = timeService;
        m_log = log;
    }

    public async Task AddAsync(string id, string username, string ip)
    {
        Client? client = await m_clientRepository.GetAsync(id);
        string group = m_groupService.GetGroup(ip);

        if (client == null)
        {
            client = new Client(
                id,
                group,
                username,
                m_timeService.GetCurrentTime());
            ILogger log = Log.ForContext("client", client);
            log.Information("Adding client");
            await m_clientRepository.AddAsync(client);
        }
        else
        {
            client = client with
            {
                Group = group,
                Username = username,
                LastSeen = m_timeService.GetCurrentTime()
            };
            ILogger log = Log.ForContext("client", client);
            log.Information("Updating client");
            await m_clientRepository.UpdateAsync(client);
        }
    }

    public Task<Client[]> ListAsync(string ip)
    {
        string group = m_groupService.GetGroup(ip);
        return m_clientRepository.ListAsync(group);
    }

    public async Task ClearOldClientsAsync()
    {
        Client[] clients = await m_clientRepository.ListAsync();
        DateTime now = m_timeService.GetCurrentTime();

        foreach (Client client in clients)
        {
            TimeSpan age = now - client.LastSeen;

            if (age < TimeSpan.FromSeconds(5))
            {
                continue;
            }

            ILogger log = Log.ForContext("client", client);
            log.Information("Removing old client");
            await m_clientRepository.RemoveAsync(client.Id);
        }
    }
}
