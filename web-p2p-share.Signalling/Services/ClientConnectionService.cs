using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Serilog;

namespace web_p2p_share.Signalling.Services;

public record ConnectionRequest(
    Client From,
    Client To,
    string Sdp);

public interface IClientConnectionService
{
    Task CreateConnectionRequestAsync(
        string fromId,
        string toId,
        string fromSdp);
    Task DeleteConnectionRequestAsync(
        string fromId,
        string toId);
    Task AnswerConnectionRequestAsync(
        string fromId,
        string toId,
        string answerSdp);
    Task<string?> GetAnswerSdpAsync(
        string fromId,
        string toId);
    Task<ConnectionRequest[]> GetConnectionRequestsAsync(
        string toId);
    Task DeleteOldConnectionsAsync();
}

public class ClientConnectionService : IClientConnectionService
{
    IClientConnectionRepository m_connectionRepository;
    IClientRepository m_clientRepository;
    ITimeService m_timeService;
    ILogger m_log;

    ILogger Log
    {
        get => m_log.ForContext<ClientConnectionService>();
    }

    public ClientConnectionService(
        IClientConnectionRepository connectionRepository,
        IClientRepository clientRepository,
        ITimeService timeService,
        ILogger log)
    {
        m_connectionRepository = connectionRepository;
        m_clientRepository = clientRepository;
        m_timeService = timeService;
        m_log = log;
    }

    public async Task CreateConnectionRequestAsync(string fromId, string toId, string fromSdp)
    {
        Client? fromClient = await m_clientRepository.GetAsync(fromId);
        Client? toClient = await m_clientRepository.GetAsync(toId);

        if (fromClient == null ||
            toClient == null)
        {
            throw new ArgumentException("Clients not found");
        }

        if (fromClient.Group != toClient.Group)
        {
            throw new ArgumentException("Clients are in different groups");
        }

        ClientConnection? connection = await m_connectionRepository.GetAsync(fromId, toId);
        ILogger log = Log.ForContext("connection", connection);

        if (connection == null)
        {
            connection = new ClientConnection(
                fromId,
                toId,
                fromSdp,
                null,
                m_timeService.GetCurrentTime());
            log.Information("Adding new connection");
            await m_connectionRepository.AddAsync(connection);
        }
        else
        {
            connection = connection with
            {
                RequestSdp = fromSdp,
                CreationTime = m_timeService.GetCurrentTime()
            };
            log.Information("Refreshing connection");
            await m_connectionRepository.ReplaceAsync(connection);
        }
    }

    public async Task DeleteConnectionRequestAsync(string fromId, string toId)
    {
        ClientConnection? connection = await m_connectionRepository.GetAsync(fromId, toId);

        if (connection == null)
        {
            throw new ArgumentException("Connection not found");
        }

        ILogger log = Log.ForContext("connection", connection);
        log.Information("Deleting connection");
        await m_connectionRepository.DeleteAsync(fromId, toId);
    }

    public async Task AnswerConnectionRequestAsync(string fromId, string toId, string answerSdp)
    {
        ClientConnection? connection = await m_connectionRepository.GetAsync(fromId, toId);

        if (connection == null)
        {
            throw new ArgumentException("Connection not found: " + fromId + " - " + toId);
        }

        if (connection.AnswerSdp != null)
        {
            throw new ArgumentException("Invalid connection state");
        }

        ILogger log = Log.ForContext("connection", connection);
        log.Information("Setting answer sdp");
        connection = connection with
        {
            AnswerSdp = answerSdp
        };
        await m_connectionRepository.ReplaceAsync(connection);
    }

    public async Task<string?> GetAnswerSdpAsync(string fromId, string toId)
    {
        ClientConnection? connection = await m_connectionRepository.GetAsync(fromId, toId);

        if (connection == null)
        {
            throw new ArgumentException("Connection not found: " + fromId + " - " + toId);
        }

        return connection.AnswerSdp;
    }

    public async Task<ConnectionRequest[]> GetConnectionRequestsAsync(string toId)
    {
        ClientConnection[] connections = await m_connectionRepository.ListAsync();
        connections = connections
            .Where(x => x.ToId == toId)
            .ToArray();
        List<ConnectionRequest> result = new List<ConnectionRequest>();
        Client? toClient = await m_clientRepository.GetAsync(toId);

        if (toClient == null)
        {
            throw new ArgumentException("Client not found");
        }

        foreach (ClientConnection connection in connections)
        {
            Client? fromClient = await m_clientRepository.GetAsync(connection.FromId);

            if (fromClient == null)
            {
                throw new Exception("Initiator client not found");
            }

            result.Add(
                new ConnectionRequest(
                    fromClient,
                    toClient,
                    connection.RequestSdp));
        }

        return result.ToArray();
    }

    public async Task DeleteOldConnectionsAsync()
    {
        ClientConnection[] connections = await m_connectionRepository.ListAsync();
        DateTime now = m_timeService.GetCurrentTime();

        foreach (ClientConnection connection in connections)
        {
            TimeSpan age = now - connection.CreationTime;

            if (age < TimeSpan.FromSeconds(30))
            {
                continue;
            }

            ILogger log = Log.ForContext("connection", connection);
            log.Information("Deleting old connection");
            await m_connectionRepository.DeleteAsync(
                connection.FromId,
                connection.ToId);
        }
    }
}
