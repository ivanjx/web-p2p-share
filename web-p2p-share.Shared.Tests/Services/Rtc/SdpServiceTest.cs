using System;
using web_p2p_share.Shared.Services.Rtc;
using Xunit;

namespace web_p2p_share.Shared.Tests.Services.Rtc;

public class SdpServiceTest
{
    SdpService m_service;

    public SdpServiceTest()
    {
        m_service = new SdpService();
    }

    [Fact]
    public void CompressDecompressTest()
    {
        string compressedSdp = m_service.Compress(RAW_SDP);
        string decompressedSdp = m_service.Decompress(compressedSdp);
        Assert.Equal(DECOMPRESSED_SDP, decompressedSdp);
    }

    const string RAW_SDP =
@"v=0
o=- 712533869939805848 2 IN IP4 127.0.0.1
s=-
t=0 0
a=group:BUNDLE 0
a=extmap-allow-mixed
a=msid-semantic: WMS
m=application 9 UDP/DTLS/SCTP webrtc-datachannel
c=IN IP4 0.0.0.0
a=candidate:2088448403 1 udp 2113937151 35a1a58f-660b-4ac6-b6dd-7c3ae393c191.local 55103 typ host generation 0 network-cost 999
a=candidate:2088448403 1 udp 2113937151 aaaaaaaa-660b-4ac6-aaaa-7c3ae393c191.local 55555 typ host generation 0 network-id 1 network-cost 10
a=candidate:2083444403 1 tcp 2113937151 35a1a58f-aaaa-4ac6-ffff-7c3ae393c191.local 12345 typ relay generation 0 network-cost 999
a=ice-ufrag:xzeC
a=ice-pwd:4Kpo3VNIcOmI/MEKlOybssx6
a=ice-options:trickle
a=fingerprint:sha-256 E6:0F:B6:B5:C1:79:A9:C1:EE:BC:2A:32:7B:2B:88:5F:5B:12:24:DA:EF:BA:9D:E9:4D:DF:60:17:D5:D3:AA:25
a=setup:actpass
a=mid:0
a=sctp-port:5000
a=max-message-size:262144
";
    const string DECOMPRESSED_SDP =
@"v=0
o=- 712533869939805848 2 IN IP4 127.0.0.1
s=-
t=0 0
a=group:BUNDLE 0
a=extmap-allow-mixed
a=msid-semantic: WMS
m=application 9 UDP/DTLS/SCTP webrtc-datachannel
c=IN IP4 0.0.0.0
a=candidate:2088448403 1 udp 2113937151 35a1a58f-660b-4ac6-b6dd-7c3ae393c191.local 55103 typ host generation 0 network-cost 999
a=candidate:2088448403 1 udp 2113937151 aaaaaaaa-660b-4ac6-aaaa-7c3ae393c191.local 55555 typ host generation 0 network-id 1 network-cost 10
a=ice-ufrag:xzeC
a=ice-pwd:4Kpo3VNIcOmI/MEKlOybssx6
a=ice-options:trickle
a=fingerprint:sha-256 E6:0F:B6:B5:C1:79:A9:C1:EE:BC:2A:32:7B:2B:88:5F:5B:12:24:DA:EF:BA:9D:E9:4D:DF:60:17:D5:D3:AA:25
a=setup:actpass
a=mid:0
a=sctp-port:5000
a=max-message-size:262144
";
}
