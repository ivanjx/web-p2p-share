# Build.
FROM mcr.microsoft.com/dotnet/sdk:8.0.100-bookworm-slim-amd64 as build
RUN dpkg --add-architecture arm64 && \
    apt-get update && \
    apt-get install -y \
    clang zlib1g-dev zlib1g-dev:arm64 \
    binutils-aarch64-linux-gnu gcc-aarch64-linux-gnu
COPY ./web-p2p-share.Signalling /web-p2p-share.Signalling
WORKDIR /web-p2p-share.Signalling
ARG TARGETPLATFORM
RUN ARCH=$(echo $TARGETPLATFORM | cut -d '/' -f 2) && \
    PREFIX=$(echo $ARCH | awk '{if ($0 == "arm64") print "aarch64-linux-gnu-";}') && \
    dotnet publish \
    -c Release \
    -r linux-$ARCH \
    -p:ObjCopyName=${PREFIX}objcopy \
    -o /output

# Runtime.
FROM mcr.microsoft.com/dotnet/runtime-deps:8.0.0-bookworm-slim as runtime
COPY --from=build /output /app
ENTRYPOINT [ "/app/web-p2p-share.Signalling" ]
