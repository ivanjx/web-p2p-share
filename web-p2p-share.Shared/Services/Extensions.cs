using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization.Metadata;
using System.Threading.Tasks;

namespace web_p2p_share.Shared.Services;

public static class Extensions
{
    public static T? FirstOrDefault<T>(this IEnumerable<T> arr)
    {
        return Enumerable.FirstOrDefault(arr);
    }
    
    static string GetApi(string path)
    {
#if DEBUG
        return "https://p2pshare.ivanjkara.net/api" + path;
#else
        return "/api" + path;
#endif
    }

    public static async Task<T?> GetAsync<T>(
        this HttpClient http,
        string path,
        JsonTypeInfo<T> tinfo)
    {
        HttpResponseMessage message = await http.GetAsync(
            GetApi(path));
        return JsonSerializer.Deserialize(
            await message.Content.ReadAsStringAsync(),
            tinfo);
    }

    static string GetParameter(Dictionary<string, string> query)
    {
        StringBuilder resultBuilder = new StringBuilder();

        foreach (string key in query.Keys)
        {
            resultBuilder.AppendFormat(
                "{0}={1}&",
                key,
                Uri.EscapeDataString(query[key]));
        }

        return resultBuilder.ToString(
            0,
            resultBuilder.Length - 1);
    }

    public static async Task<T?> GetAsync<T>(
        this HttpClient http,
        string path,
        Dictionary<string, string> query,
        JsonTypeInfo<T> tinfo)
    {
        HttpResponseMessage response = await http.GetAsync(
            GetApi(path) + "?" + GetParameter(query));
        return JsonSerializer.Deserialize(
            await response.Content.ReadAsStringAsync(),
            tinfo);
    }

    public static async Task PostAsync<D>(
        this HttpClient http,
        string path,
        D data,
        JsonTypeInfo<D> dinfo)
    {
        HttpContent request = new StringContent(
            JsonSerializer.Serialize(data, dinfo),
            Encoding.UTF8,
            "application/json");
        await http.PostAsync(
            GetApi(path),
            request);
    }

    public static async Task<T?> PostAsync<T, D>(
        this HttpClient http,
        string path,
        D data,
        JsonTypeInfo<T?> tinfo,
        JsonTypeInfo<D> dinfo)
    {
        HttpContent request = new StringContent(
            JsonSerializer.Serialize(data, dinfo),
            Encoding.UTF8,
            "application/json");
        HttpResponseMessage response = await http.PostAsync(
            GetApi(path),
            request);
        return JsonSerializer.Deserialize(
            await response.Content.ReadAsStringAsync(),
            tinfo);
    }
}
