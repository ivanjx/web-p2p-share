using System;

namespace web_p2p_share.Shared.Services.Media;

public interface ICanvasHandle : IDisposable
{
}

public interface ICanvasService
{
    ICanvasHandle Init(string canvasId);
    double GetHeight(ICanvasHandle handle);
    double GetWidth(ICanvasHandle handle);
    void Draw(
        ICanvasHandle handle,
        byte[] data);
}
