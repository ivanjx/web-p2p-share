using System;
using System.Threading.Tasks;

namespace web_p2p_share.Shared.Services.Media;

public interface IWebCamHandle : IDisposable
{
}

public interface IWebCamService
{
    bool IsSupported();
    Task<IWebCamHandle> StartAsync(
        string videoTagId,
        string canvasTagId);
    void Stop(IWebCamHandle handle);
    byte[] GetImage(IWebCamHandle handle);
}
