using System;
using System.Threading.Tasks;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using ZXing;
using ZXing.Common;

namespace web_p2p_share.Shared.Services.Media;

public interface IQrService
{
    byte[] Encode(string data, int width, int height);
    string? Decode(byte[] pixel, int width, int height);
}

public class QrService : IQrService
{
    public string? Decode(byte[] pixel, int width, int height)
    {
        Image<Rgba32> image = Image.LoadPixelData<Rgba32>(
            pixel,
            width,
            height);
        var reader = new ZXing.ImageSharp.BarcodeReader<Rgba32>();
        reader.Options = new DecodingOptions()
        {
            TryHarder = true,
            PossibleFormats = new[] { BarcodeFormat.QR_CODE }
        };
        return reader.Decode(image)?.Text;
    }

    public byte[] Encode(string data, int width, int height)
    {
        var writer = new ZXing.ImageSharp.BarcodeWriter<Rgba32>();
        writer.Format = BarcodeFormat.QR_CODE;
        writer.Options = new EncodingOptions()
        {
            Width = width,
            Height = height
        };
        Image<Rgba32> image = writer.Write(data);
        Size size = image.Size;
        byte[] result = new byte[4 * size.Width * size.Height];
        image.CopyPixelDataTo(result);
        return result;
    }
}
