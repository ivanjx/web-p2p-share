using System;

namespace web_p2p_share.Shared.Services.Media;

public interface IWakeLockService
{
    void AcquireLock();
}
