using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using web_p2p_share.Shared.Services.Rtc;

namespace web_p2p_share.Shared.Services.Transfer;

public interface IServerService
{
    void AddFiles(
        string receiverId,
        params IFileHandle[] files);
    void RemoveFile(
        string receiverId,
        string fileId);
    Task ProcessCommandsAsync(string receiverId);
    Task ProcessDataAsync(string receiverId);
    void CleanDisconnectedClients();
}

public class ServerService : IServerService
{
    ServerState m_state;
    ConnectionState m_connectionState;
    IRtcService m_rtcService;
    IStorageService m_storageService;

    public ServerService(
        ServerState state,
        ConnectionState connectionState,
        IRtcService rtcService,
        IStorageService storageService)
    {
        m_state = state;
        m_connectionState = connectionState;
        m_rtcService = rtcService;
        m_storageService = storageService;
    }

    public void AddFiles(string receiverId, params IFileHandle[] files)
    {
        ServerFileInfo[] newFiles = files
            .Where(x => x.Size > 0)
            .Select(x =>
                new ServerFileInfo(
                    Guid.NewGuid().ToString(),
                    x.Name,
                    x.Size,
                    x))
            .ToArray();
        ServerFileList? list = m_state.FileLists
            .Where(x => x.ReceiverId == receiverId)
            .FirstOrDefault();
        
        if (list == null)
        {
            list = new ServerFileList(
                receiverId,
                newFiles);
            m_state.FileLists = m_state.FileLists
                .Append(list)
                .ToArray();
        }
        else
        {
            list = list with
            {
                Files = list.Files
                    .Concat(newFiles)
                    .ToArray()
            };
            m_state.FileLists = m_state.FileLists
                .Select(x => x.ReceiverId == receiverId ? list : x)
                .ToArray();
        }
    }

    public void RemoveFile(string receiverId, string fileId)
    {
        ServerFileInfo[]? files = m_state.FileLists
            .Where(x => x.ReceiverId == receiverId)
            .Select(x => x.Files)
            .FirstOrDefault();
        
        if (files == null)
        {
            return; // Impossible.
        }

        ServerFileInfo? file = files
            .Where(x => x.Id == fileId)
            .FirstOrDefault();
        
        if (file == null)
        {
            return; // Impossible.
        }

        file.Handle.Dispose();
        files = files
            .Where(x => x.Id != fileId)
            .ToArray();
        m_state.FileLists = m_state.FileLists
            .Select(x =>
            {
                if (x.ReceiverId == receiverId)
                {
                    return x with
                    {
                        Files = files
                    };
                }
                
                return x;
            })
            .ToArray();
    }

    public async Task ProcessCommandsAsync(string receiverId)
    {
        ReceiverConnection? connection = m_connectionState.Receivers
            .Where(x => x.Receiver.Id == receiverId)
            .FirstOrDefault();
        
        if (connection == null)
        {
            throw new Exception("Connection not found: " + receiverId);
        }

        IRtcConnection? rtc = connection.Connection;

        if (rtc == null)
        {
            throw new Exception("Connection not open: " + receiverId);
        }

        byte[] requestLenBuff = await m_rtcService.ReadAsync(
            rtc,
            true,
            4);
        int requestLen = BitConverter.ToInt32(requestLenBuff);
        byte[] requestBuff = await m_rtcService.ReadAsync(
            rtc,
            true,
            requestLen);
        string requestStr = Encoding.ASCII.GetString(requestBuff);
        Console.WriteLine("{0}: CMD {1}", receiverId, requestStr);

        if (requestStr == "listfiles")
        {
            FileInfoResponse[]? response = m_state.FileLists
                .Where(x => x.ReceiverId == receiverId).FirstOrDefault()?.Files
                .Select(x => new FileInfoResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Size = x.Size
                })
                .ToArray();
            
            if (response == null)
            {
                response = new FileInfoResponse[0];
            }

            string responseStr = JsonSerializer.Serialize(
                response,
                ServerContext.Default.FileInfoResponseArray);
            byte[] responseBuff = Encoding.Unicode.GetBytes(responseStr);
            Console.WriteLine("Responding with file list to: {0}", receiverId);
            await m_rtcService.WriteAsync(
                rtc,
                true,
                BitConverter.GetBytes(responseBuff.Length));
            await m_rtcService.WriteAsync(
                rtc,
                true,
                responseBuff);
        }
        else
        {
            throw new Exception("Invalid request from receiver");
        }
    }

    public async Task ProcessDataAsync(string receiverId)
    {
        ReceiverConnection? connection = m_connectionState.Receivers
            .Where(x => x.Receiver.Id == receiverId)
            .FirstOrDefault();
        
        if (connection == null)
        {
            throw new Exception("Connection not found: " + receiverId);
        }

        IRtcConnection? rtc = connection.Connection;

        if (rtc == null)
        {
            throw new Exception("Connection not open: " + receiverId);
        }

        byte[] requestLenBuff = await m_rtcService.ReadAsync(
            rtc,
            false,
            4);
        int requestLen = BitConverter.ToInt32(requestLenBuff);
        byte[] requestBuff = await m_rtcService.ReadAsync(
            rtc,
            false,
            requestLen);
        string requestStr = Encoding.ASCII.GetString(requestBuff);
        Console.WriteLine("{0}: DATA {1}", receiverId, requestStr);

        string[] requestStrSplit = requestStr.Split(':');

        if (requestStrSplit.Length != 3)
        {
            throw new Exception("Invalid request from receiver");
        }

        string fileId = requestStrSplit[0];
        long fileOffset;

        if (!long.TryParse(requestStrSplit[1], out fileOffset))
        {
            throw new Exception("Invalid request from receiver");
        }

        int readSize;

        if (!int.TryParse(requestStrSplit[2], out readSize))
        {
            throw new Exception("Invalid request from receiver");
        }

        ServerFileInfo? file = m_state.FileLists
            .Where(x => x.ReceiverId == receiverId).FirstOrDefault()?.Files
            .FirstOrDefault(x => x.Id == fileId);
        
        if (file == null)
        {
            Console.WriteLine(
                "File {0} no longer exists for receiver: {1}",
                fileId,
                receiverId);
            await m_rtcService.WriteAsync(
                rtc,
                false,
                BitConverter.GetBytes(0));
            return;
        }

        if (file.Size < fileOffset + readSize)
        {
            throw new Exception("Invalid size request from receiver");
        }

        byte[] responseBuff = await m_storageService.ReadAsync(
            file.Handle,
            fileOffset,
            readSize);
        Console.WriteLine(
            "Responding {0} from {1} size {2} to {3}",
            fileId,
            fileOffset,
            responseBuff.Length,
            receiverId);
        await m_rtcService.WriteAsync(
            rtc,
            false,
            BitConverter.GetBytes(responseBuff.Length));
        await m_rtcService.WriteAsync(
            rtc,
            false,
            responseBuff);
    }

    public void CleanDisconnectedClients()
    {
        List<string> disconnectedReceivers = new List<string>();

        foreach (ServerFileList list in m_state.FileLists)
        {
            IRtcConnection? connection = m_connectionState.Receivers
                .Where(x => x.Receiver.Id == list.ReceiverId)
                .Select(x => x.Connection)
                .Where(x => x != null && x.IsOpen)
                .FirstOrDefault();
            
            if (connection != null)
            {
                continue;
            }

            Console.WriteLine("Disconnected from receiver: {0}", list.ReceiverId);
            disconnectedReceivers.Add(list.ReceiverId);

            foreach (IFileHandle file in list.Files.Select(x => x.Handle))
            {
                file.Dispose();
            }
        }

        m_state.FileLists = m_state.FileLists
            .Where(x => !disconnectedReceivers.Contains(x.ReceiverId))
            .ToArray();
    }

    public record FileInfoResponse
    {
        [JsonPropertyName("id")]
        public string Id
        {
            get;
            set;
        }

        [JsonPropertyName("name")]
        public string Name
        {
            get;
            set;
        }

        [JsonPropertyName("size")]
        public long Size
        {
            get;
            set;
        }

        public FileInfoResponse()
        {
            Id = "";
            Name = "";
        }
    }
}

[JsonSerializable(typeof(ServerService.FileInfoResponse[]))]
internal partial class ServerContext : JsonSerializerContext
{
}
