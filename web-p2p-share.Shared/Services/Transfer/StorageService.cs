using System;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace web_p2p_share.Shared.Services.Transfer;

public interface IFileHandle : IDisposable
{
    string Name
    {
        get;
    }

    long Size
    {
        get;
    }
}

public interface IBlob : IDisposable
{
}

public interface IStorageService
{
    Task<byte[]> ReadAsync(
        IFileHandle handle,
        long offset,
        int length);
    Task<IBlob> GetFileBlobAsync(string fileId);
    Task DownloadFileBlobAsync(
        IBlob blob,
        string name);
    Task<string> InsertChunkAsync(
        string fileId,
        long offset,
        byte[] data);
    Task DeleteFileChunksAsync(string fileId);
    Task DeleteFileChunksAsync();
}
