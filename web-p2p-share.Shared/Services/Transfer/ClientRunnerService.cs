using System;
using System.Threading.Tasks;

namespace web_p2p_share.Shared.Services.Transfer;

public interface IClientRunnerService
{
    void Start();
}

public class ClientRunnerService : IClientRunnerService
{
    IClientService m_clientSrvice;

    public ClientRunnerService(
        IClientService clientService)
    {
        m_clientSrvice = clientService;
    }

    public async void Start()
    {
        while (true)
        {
            m_clientSrvice.CleanDisconnectedServers();
            await Task.Delay(TimeSpan.FromSeconds(5));
        }
    }
}
