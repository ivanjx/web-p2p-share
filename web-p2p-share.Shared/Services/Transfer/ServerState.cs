using System;

namespace web_p2p_share.Shared.Services.Transfer;

public record ServerFileInfo(
    string Id,
    string Name,
    long Size,
    IFileHandle Handle);

public record ServerFileList(
    string ReceiverId,
    ServerFileInfo[] Files);

public class ServerState
{
    ServerFileList[] m_fileLists;

    public Action? FileListsChanged;

    public ServerFileList[] FileLists
    {
        get => m_fileLists;
        set
        {
            m_fileLists = value;
            FileListsChanged?.Invoke();
        }
    }

    public ServerState()
    {
        m_fileLists = new ServerFileList[0];
    }
}
