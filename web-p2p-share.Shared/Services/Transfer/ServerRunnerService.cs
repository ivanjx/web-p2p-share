using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using web_p2p_share.Shared.Services.Rtc;

namespace web_p2p_share.Shared.Services.Transfer;

public interface IServerRunnerService
{
    void Start();
}

public class ServerRunnerService : IServerRunnerService
{
    static Dictionary<string, Task> m_commandTasks;
    static Dictionary<string, Task> m_dataTasks;

    static ServerRunnerService()
    {
        m_commandTasks = new Dictionary<string, Task>();
        m_dataTasks = new Dictionary<string, Task>();
    }

    ConnectionState m_connectionState;
    IServerService m_serverService;

    public ServerRunnerService(
        ConnectionState connectionState,
        IServerService serverService)
    {
        m_connectionState = connectionState;
        m_serverService = serverService;
    }

    public async void Start()
    {
        while (true)
        {
            // Spin up command processing runner.
            foreach (var receiver in m_connectionState.Receivers)
            {
                if (receiver.Connection == null ||
                    !receiver.Connection.IsOpen)
                {
                    // Not connected.
                    continue;
                }

                Task? commandTask;

                if (m_commandTasks.TryGetValue(receiver.Receiver.Id, out commandTask) &&
                    !commandTask.IsCompleted)
                {
                    // Already processing.
                    continue;
                }

                Console.WriteLine(
                    "Spinning up command processing runner for receiver: {0}",
                    receiver.Receiver.Id);
                commandTask = ProcessCommandsAsync(receiver.Receiver.Id);
                m_commandTasks[receiver.Receiver.Id] = commandTask;
            }

            // Spin up command processing runner.
            foreach (var receiver in m_connectionState.Receivers)
            {
                if (receiver.Connection == null ||
                    !receiver.Connection.IsOpen)
                {
                    // Not connected.
                    continue;
                }

                Task? dataTask;

                if (m_dataTasks.TryGetValue(receiver.Receiver.Id, out dataTask) &&
                    !dataTask.IsCompleted)
                {
                    // Already processing.
                    continue;
                }

                Console.WriteLine(
                    "Spinning up data processing runner for receiver: {0}",
                    receiver.Receiver.Id);
                dataTask = ProcessDataAsync(receiver.Receiver.Id);
                m_dataTasks[receiver.Receiver.Id] = dataTask;
            }

            m_serverService.CleanDisconnectedClients();

            await Task.Delay(TimeSpan.FromSeconds(3));
        }
    }

    async Task ProcessCommandsAsync(string receiverId)
    {
        while (true)
        {
            try
            {
                await m_serverService.ProcessCommandsAsync(receiverId); // Blocking.
            }
            catch (Exception ex)
            {
                Console.WriteLine(
                    "Error processing commands {0}: {1}",
                    receiverId,
                    ex);
                break;
            }
        }
    }

    async Task ProcessDataAsync(string receiverId)
    {
        while (true)
        {
            try
            {
                await m_serverService.ProcessDataAsync(receiverId); // Blocking.
            }
            catch (Exception ex)
            {
                Console.WriteLine(
                    "Error processing data {0}: {1}",
                    receiverId,
                    ex);
                break;
            }
        }
    }
}
