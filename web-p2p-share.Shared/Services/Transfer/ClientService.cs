using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;
using web_p2p_share.Shared.Services.Rtc;

namespace web_p2p_share.Shared.Services.Transfer;

public interface IClientService
{
    Task LoadFilesAsync(string senderId);
    void StartDownload(
        string senderId,
        string fileId);
    void StopDownload(
        string senderId,
        string fileId);
    Task SaveAsync(
        string senderId,
        string fileId);
    void CleanDisconnectedServers();
}

public class ClientService : IClientService
{
    const int FILE_READ_SIZE = 256 * 1024;

    ClientState m_state;
    ConnectionState m_connectionState;
    IRtcService m_rtcService;
    IStorageService m_storageService;

    public ClientService(
        ClientState state,
        ConnectionState connectionState,
        IRtcService rtcService,
        IStorageService storageService)
    {
        m_state = state;
        m_connectionState = connectionState;
        m_rtcService = rtcService;
        m_storageService = storageService;
    }

    public async Task LoadFilesAsync(string senderId)
    {
        SenderConnection? connection = m_connectionState.Senders
            .Where(x => x.Request.Peer.Id == senderId)
            .FirstOrDefault();
        
        if (connection == null)
        {
            throw new Exception("Connection not found: " + senderId);
        }

        IRtcConnection? rtc = connection.Connection;

        if (rtc == null)
        {
            throw new Exception("Connection not open: " + senderId);
        }

        Console.WriteLine("Sending load files request: {0}", senderId);
        byte[] requestBuff = Encoding.ASCII.GetBytes("listfiles");
        byte[] requestLenBuff = BitConverter.GetBytes(requestBuff.Length);
        await m_rtcService.WriteAsync(
            rtc,
            true,
            requestLenBuff);
        await m_rtcService.WriteAsync(
            rtc,
            true,
            requestBuff);
        
        Console.WriteLine("Reading load files response: {0}", senderId);
        byte[] responseLenBuff = await m_rtcService.ReadAsync(
            rtc,
            true,
            4);
        int responseLen = BitConverter.ToInt32(responseLenBuff);
        byte[] responseBuff = await m_rtcService.ReadAsync(
            rtc,
            true,
            responseLen);
        FileInfoResponse[]? response = JsonSerializer.Deserialize(
            Encoding.Unicode.GetString(responseBuff),
            ClientContext.Default.FileInfoResponseArray);
        
        if (response == null)
        {
            throw new Exception("Invalid response from sender");
        }

        Console.WriteLine("Updating client state: {0}", senderId);
        ClientFileInfo[] files = response
            .Select(x =>
            {
                if (x.Id == null ||
                    x.Name == null ||
                    x.Size == 0)
                {
                    throw new Exception("Invalid response from sender");
                }

                return new ClientFileInfo(
                    x.Id,
                    x.Name,
                    x.Size);
            })
            .ToArray();

        ClientFileList? fileList = m_state.FileLists
            .Where(x => x.SenderId == senderId)
            .FirstOrDefault();
        
        if (fileList == null)
        {
            fileList = new ClientFileList(
                senderId,
                files);
            m_state.FileLists = m_state.FileLists
                .Append(fileList)
                .ToArray();
        }
        else
        {
            fileList = fileList with
            {
                Files = files
            };
            m_state.FileLists = m_state.FileLists
                .Select(x =>
                {
                    if (x.SenderId == senderId)
                    {
                        return fileList;
                    }

                    return x;
                })
                .ToArray();
        }
    }

    public async void StartDownload(string senderId, string fileId)
    {
        // Validating for duplicate transfer.
        ClientFileTransfer? transfer = m_state.FileTransfers
            .Where(x => 
                x.SenderId == senderId &&
                x.File.Id == fileId)
            .FirstOrDefault();
        
        if (transfer != null)
        {
            Console.WriteLine("File {0} already downloading: {1}", fileId, senderId);
            return;
        }

        // Getting connection.
        SenderConnection? connection = m_connectionState.Senders
            .Where(x => x.Request.Peer.Id == senderId)
            .FirstOrDefault();
        
        if (connection == null)
        {
            throw new Exception("Connection not found: " + senderId);
        }

        IRtcConnection? rtc = connection.Connection;

        if (rtc == null)
        {
            throw new Exception("Connection not open: " + senderId);
        }

        // Initializing transfer semaphore if required.
        SemaphoreSlim? transferSemaphore = m_state.FileTransfers
            .Where(x =>
                x.SenderId == senderId)
            .Select(x => x.TransferSemaphore)
            .FirstOrDefault();
        
        if (transferSemaphore == null)
        {
            Console.WriteLine("Initializing new transfer semaphore: {0}", senderId);
            transferSemaphore = new SemaphoreSlim(1);
        }

        // Validate file info.
        ClientFileInfo? file = m_state.FileLists
            .Where(x => x.SenderId == senderId)
            .Select(x => x.Files)
            .FirstOrDefault()?
            .Where(x => x.Id == fileId)
            .FirstOrDefault();
        
        if (file == null)
        {
            // Weird but ok.
            return;
        }

        // Start download.
        CancellationTokenSource cts = new CancellationTokenSource();
        transfer = new ClientFileTransfer(
            senderId,
            file,
            0,
            transferSemaphore,
            cts);
        m_state.FileTransfers = m_state.FileTransfers
            .Append(transfer)
            .ToArray();
        await transferSemaphore.WaitAsync(); // Use proper queueing in the future!
        Console.WriteLine("Enter download semaphore: {0}", senderId);

        try
        {
            while (transfer.ReceivedSize < file.Size)
            {
                cts.Token.ThrowIfCancellationRequested();

                Console.WriteLine(
                    "Sending request of file {0} at {1}: {2}",
                    fileId,
                    transfer.ReceivedSize,
                    senderId);
                string requestStr = string.Format(
                    "{0}:{1}:{2}",
                    fileId,
                    transfer.ReceivedSize,
                    Math.Min(FILE_READ_SIZE, file.Size - transfer.ReceivedSize));
                byte[] requestBuff = Encoding.ASCII.GetBytes(requestStr);
                byte[] requestLenBuff = BitConverter.GetBytes(requestBuff.Length);
                await m_rtcService.WriteAsync(
                    rtc,
                    false,
                    requestLenBuff);
                await m_rtcService.WriteAsync(
                    rtc,
                    false,
                    requestBuff);
                
                Console.WriteLine(
                    "Reading download request of file {0} at {1}: {2}",
                    fileId,
                    transfer.ReceivedSize,
                    senderId);
                byte[] responseLenBuff = await m_rtcService.ReadAsync(
                    rtc,
                    false,
                    4);
                int responseLen = BitConverter.ToInt32(responseLenBuff);

                if (responseLen == 0)
                {
                    m_state.FileTransfers = m_state.FileTransfers
                        .Where(x => !(
                            x.SenderId == senderId &&
                            x.File.Id == fileId))
                        .ToArray();
                    throw new Exception("File transfer cancelled by sender");
                }

                byte[] responseBuff = await m_rtcService.ReadAsync(
                    rtc,
                    false,
                    responseLen);
                Console.WriteLine(
                    "Saving file chunk {0} at {1}: {2}",
                    fileId,
                    transfer.ReceivedSize,
                    senderId);
                await m_storageService.InsertChunkAsync(
                    fileId,
                    transfer.ReceivedSize,
                    responseBuff);

                transfer = transfer with
                {
                    ReceivedSize = transfer.ReceivedSize + responseLen
                };
                m_state.FileTransfers = m_state.FileTransfers
                    .Select(x =>
                    {
                        if (x.SenderId == senderId &&
                            x.File.Id == fileId)
                        {
                            return transfer;
                        }

                        return x;
                    })
                    .ToArray();
            }

            Console.WriteLine("Transfer file {0} completed: {1}", fileId, senderId);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Download error {0}: {1}", senderId, ex);
            Console.WriteLine("Clearing file chunks: {0}", fileId);
            await m_storageService.DeleteFileChunksAsync(fileId);
        }
        finally
        {
            cts.Dispose();

            Console.WriteLine("Exit download semaphore: {0}", senderId);
            transferSemaphore.Release();
        }
    }

    public void StopDownload(string senderId, string fileId)
    {
        ClientFileTransfer? transfer = m_state.FileTransfers
            .Where(x =>
                x.SenderId == senderId &&
                x.File.Id == fileId)
            .FirstOrDefault();
        
        if (transfer == null ||
            transfer.ReceivedSize == transfer.File.Size ||
            transfer.CancellationTokenSource == null)
        {
            // Nothing to cancel.
            return;
        }

        Console.WriteLine("Cancelling transfer of {0}: {1}", fileId, senderId);
        transfer.CancellationTokenSource.Cancel();
        m_state.FileTransfers = m_state.FileTransfers
            .Where(x => !(
                x.SenderId == senderId &&
                x.File.Id == fileId))
            .ToArray();
    }

    public async Task SaveAsync(string senderId, string fileId)
    {
        ClientFileTransfer? transfer = m_state.FileTransfers
            .Where(x =>
                x.SenderId == senderId &&
                x.File.Id == fileId)
            .FirstOrDefault();
        
        if (transfer == null ||
            transfer.ReceivedSize < transfer.File.Size) // Not done.
        {
            return;
        }

        try
        {
            Console.WriteLine("Saving downloaded file {0}: {1}", fileId, senderId);
            using IBlob blob = await m_storageService.GetFileBlobAsync(fileId);
            await m_storageService.DownloadFileBlobAsync(
                blob,
                transfer.File.Name);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Error saving file {0}: {1}", fileId, ex);
        }
    }

    public void CleanDisconnectedServers()
    {
        List<string> disconnectedSenders = new List<string>();

        foreach (ClientFileList list in m_state.FileLists)
        {
            IRtcConnection? connection = m_connectionState.Senders
                .Where(x => x.Request.Peer.Id == list.SenderId)
                .Select(x => x.Connection)
                .Where(x => x != null && x.IsOpen)
                .FirstOrDefault();
            
            if (connection != null)
            {
                continue;
            }

            Console.WriteLine("Disconnected from sender: {0}", list.SenderId);
            disconnectedSenders.Add(list.SenderId);
        }

        m_state.FileLists = m_state.FileLists
            .Where(x => !disconnectedSenders.Contains(x.SenderId))
            .ToArray();
    }

    public record FileInfoResponse
    {
        [JsonPropertyName("id")]
        public string? Id
        {
            get;
            set;
        }

        [JsonPropertyName("name")]
        public string? Name
        {
            get;
            set;
        }

        [JsonPropertyName("size")]
        public long Size
        {
            get;
            set;
        }
    }
}

[JsonSerializable(typeof(ClientService.FileInfoResponse[]))]
internal partial class ClientContext : JsonSerializerContext
{
}
