using System;
using System.Threading;

namespace web_p2p_share.Shared.Services.Transfer;

public record ClientFileInfo(
    string Id,
    string Name,
    long Size);

public record ClientFileList(
    string SenderId,
    ClientFileInfo[] Files);

public record ClientFileTransfer(
    string SenderId,
    ClientFileInfo File,
    long ReceivedSize,
    SemaphoreSlim TransferSemaphore,
    CancellationTokenSource? CancellationTokenSource);

public class ClientState
{
    ClientFileList[] m_fileLists;
    ClientFileTransfer[] m_fileTransfers;

    public Action? FileListsChanged;
    public Action? FileTransfersChanged;

    public ClientFileList[] FileLists
    {
        get => m_fileLists;
        set
        {
            m_fileLists = value;
            FileListsChanged?.Invoke();
        }
    }

    public ClientFileTransfer[] FileTransfers
    {
        get => m_fileTransfers;
        set
        {
            m_fileTransfers = value;
            FileTransfersChanged?.Invoke();
        }
    }

    public ClientState()
    {
        m_fileLists = new ClientFileList[0];
        m_fileTransfers = new ClientFileTransfer[0];
    }
}
