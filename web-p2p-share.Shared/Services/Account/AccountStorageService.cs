using System;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace web_p2p_share.Shared.Services.Account;

public record UserObject
{
    [JsonPropertyName("id")]
    public string Id
    {
        get;
        set;
    }

    [JsonPropertyName("username")]
    public string Username
    {
        get;
        set;
    }

    public UserObject()
    {
        Id = "";
        Username = "";
    }
}

public interface IAccountStorageService
{
    Task SetAsync(UserObject user);
    Task<UserObject?> GetAsync();
}
