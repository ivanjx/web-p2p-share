using System;
using System.Linq;
using System.Threading.Tasks;

namespace web_p2p_share.Shared.Services.Account;

public interface IAccountService
{
    Task InitAsync();
    Task ChangeUsernameAsync(string newUsername);
}

public class AccountService : IAccountService
{
    AccountState m_state;
    IAccountStorageService m_storageService;

    public AccountService(
        AccountState state,
        IAccountStorageService storageService)
    {
        m_state = state;
        m_storageService = storageService;
    }

    public async Task InitAsync()
    {
        Console.WriteLine("Reading saved user info");
        UserObject? user = await m_storageService.GetAsync();

        if (user == null)
        {
            string id = Guid.NewGuid().ToString();
            user = new UserObject()
            {
                Id = id,
                Username = string.Format(
                    "user-{0}",
                    id.Split('-').Last())
            };
            Console.WriteLine("Creating default user");
            Console.WriteLine(user);
            await m_storageService.SetAsync(user);
        }

        m_state.Id = user.Id;
        m_state.Username = user.Username;
    }

    public async Task ChangeUsernameAsync(string newUsername)
    {
        UserObject? user = await m_storageService.GetAsync();

        if (user == null)
        {
            throw new Exception("User is null"); // Impossible.
        }

        if (newUsername == "" ||
            newUsername == user.Username)
        {
            Console.WriteLine("Username not changed");
            return;
        }

        user = user with
        {
            Username = newUsername
        };
        Console.WriteLine("Saving new username: {0}", user);
        await m_storageService.SetAsync(user);
        m_state.Username = newUsername;
    }
}
