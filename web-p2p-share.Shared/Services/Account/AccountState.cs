using System;

namespace web_p2p_share.Shared.Services.Account;

public class AccountState
{
    public event Action? AccountChanged;

    string m_id;
    string m_username;

    public string Id
    {
        get => m_id;
        set
        {
            m_id = value;
            AccountChanged?.Invoke();
        }
    }

    public string Username
    {
        get => m_username;
        set
        {
            m_username = value;
            AccountChanged?.Invoke();
        }
    }

    public AccountState()
    {
        m_id = "";
        m_username = "";
    }
}
