using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace web_p2p_share.Shared.Services.Rtc;

public interface ISdpService
{
    string Compress(string sdp);
    string Decompress(string compressedSdp);
}

public class SdpService : ISdpService
{
    // Credit: https://webrtchacks.com/the-minimum-viable-sdp/

    string CompressHash(string hashStr)
    {
        hashStr = hashStr.Replace(":", "");
        byte[] hash = new byte[hashStr.Length / 2];

        for (int i = 0; i < hash.Length; ++i)
        {
            byte b = byte.Parse(
                hashStr.Substring(i * 2, 2),
                NumberStyles.HexNumber);
            hash[i] = b;
        }

        return Convert.ToBase64String(hash);
    }

    string CompressCandidate(string candidate)
    {
        // Parsing candidate line.
        string[] candidateSplit = candidate.Split(' ');
        string foundation = candidateSplit[0];
        string priority = candidateSplit[3];
        string address = candidateSplit[4];
        string port = candidateSplit[5];
        string networkCost = candidateSplit.Last();
        string? networkId = null;

        if (candidate.Contains("network-id"))
        {
            int index = candidate.IndexOf(
                "network-id ",
                StringComparison.Ordinal);
            networkId = candidate.Substring(index + 11);
            networkId = networkId.Split(' ').First();
        }

        // Compressing foundation.
        uint foundationInt = uint.Parse(foundation);
        foundation = Convert.ToBase64String(
            BitConverter.GetBytes(foundationInt));

        // Compressing priority.
        uint priorityInt = uint.Parse(priority);
        priority = Convert.ToBase64String(
            BitConverter.GetBytes(priorityInt));
        
        // Building.
        StringBuilder resultBuilder = new StringBuilder();
        resultBuilder.Append($"{foundation} ");
        resultBuilder.Append($"{priority} ");
        resultBuilder.Append($"{address} ");
        resultBuilder.Append($"{port} ");

        if (networkId == null)
        {
            resultBuilder.Append(networkCost);
        }
        else
        {
            resultBuilder.Append($"{networkCost} ");
            resultBuilder.Append(networkId);
        }

        return resultBuilder.ToString();
    }

    public string Compress(string sdp)
    {
        // Parsing.
        using StringReader reader = new StringReader(sdp);
        string? iceUfrag = null;
        string? icePwd = null;
        string? hash = null;
        bool? isOffer = null;
        List<string> candidates = new List<string>();
        
        while (true)
        {
            string? line = reader.ReadLine();

            if (line == null)
            {
                break;
            }

            if (line.StartsWith("a=ice-ufrag:"))
            {
                iceUfrag = line.Substring(12);
            }
            else if (line.StartsWith("a=ice-pwd:"))
            {
                icePwd = line.Substring(10);
            }
            else if (line.StartsWith("a=fingerprint:sha-256 "))
            {
                hash = line.Substring(22);
            }
            else if (line.StartsWith("a=setup:"))
            {
                isOffer = line.Contains("actpass"); // 'active' for answer.
            }
            else if (line.StartsWith("a=candidate:"))
            {
                candidates.Add(line.Substring(12));
            }
        }

        if (iceUfrag == null)
        {
            throw new Exception("No iceUfrag");
        }

        if (icePwd == null)
        {
            throw new Exception("No icePwd");
        }

        if (hash == null)
        {
            throw new Exception("No hash");
        }

        if (isOffer == null)
        {
            throw new Exception("No offer/answer indication");
        }

        if (candidates.Count == 0)
        {
            throw new Exception("No candidates");
        }

        // Compressing hash.
        hash = CompressHash(hash);

        // Compressing candidates.
        candidates = candidates
            .Where(x =>
                !x.Contains("typ relay") && // Ignore relay candidates.
                !x.Contains("tcp")) // Ignore tcp.
            .ToList();

        for (int i = 0; i < candidates.Count; ++i)
        {
            candidates[i] = CompressCandidate(candidates[i]);
        }

        // Done.
        StringBuilder resultBuilder = new StringBuilder();
        resultBuilder.Append($"{iceUfrag};");
        resultBuilder.Append($"{icePwd};");
        resultBuilder.Append($"{hash};");
        resultBuilder.Append($"{(isOffer.Value ? "O" : "A")};");

        foreach (string candidate in candidates)
        {
            resultBuilder.Append($"{candidate};");
        }

        return resultBuilder.ToString(
            0,
            resultBuilder.Length - 1); // Remove the last ';'.
    }

    string DecompressHash(string compressedHash)
    {
        byte[] compressedHashBuff =
            Convert.FromBase64String(compressedHash);
        StringBuilder resultBuilder = new StringBuilder();

        foreach (byte b in compressedHashBuff)
        {
            resultBuilder.Append($"{b:X2}:");
        }

        return resultBuilder.ToString(
            0,
            resultBuilder.Length - 1); // Removing the last ':'.
    }

    string DecompressCandidate(string compressedCandidateSdp)
    {
        // Parsing compressed line.
        string[] candidateSdpSplit = compressedCandidateSdp.Split(' ');
        string foundation = candidateSdpSplit[0];
        string priority = candidateSdpSplit[1];
        string address = candidateSdpSplit[2];
        string port = candidateSdpSplit[3];
        string networkCost = candidateSdpSplit[4];
        string? networkId = null;

        if (candidateSdpSplit.Length == 6)
        {
            networkId = candidateSdpSplit[5];
        }

        // Decompressing foundation.
        byte[] foundationBuff = Convert.FromBase64String(foundation);
        foundation = BitConverter.ToUInt32(foundationBuff).ToString();

        // Decompressing priority.
        byte[] priorityBuff = Convert.FromBase64String(priority);
        priority = BitConverter.ToUInt32(priorityBuff).ToString();

        // Building.
        if (networkId == null)
        {
            return string.Format(
                CANDIDATE_SKELETON,
                foundation,
                priority,
                address,
                port,
                networkCost);
        }
        
        return string.Format(
            CANDIDATE_NET_SKELETON,
            foundation,
            priority,
            address,
            port,
            networkCost,
            networkId);
    }

    public string Decompress(string compressedSdp)
    {
        string[] compressedSdpSplit = compressedSdp.Split(';');
        
        if (compressedSdpSplit.Length < 5) // Minimum 1 candidate.
        {
            throw new ArgumentException("Invalid compressed sdp format");
        }

        string ufrag = compressedSdpSplit[0];
        string icePwd = compressedSdpSplit[1];
        string hash = DecompressHash(compressedSdpSplit[2]);
        string offer = compressedSdpSplit[3] == "O" ?
            "actpass" : "active";
        string[] candidateSdps =
            compressedSdpSplit.Skip(4).ToArray();
        StringBuilder candidateBuilder = new StringBuilder();

        foreach (string candidateSdp in candidateSdps)
        {
            candidateBuilder.AppendLine(
                DecompressCandidate(candidateSdp));
        }

        return string.Format(
            SDP_SKELETON,
            candidateBuilder.ToString().TrimEnd(),
            ufrag,
            icePwd,
            hash,
            offer);
    }

    const string SDP_SKELETON =
@"v=0
o=- 712533869939805848 2 IN IP4 127.0.0.1
s=-
t=0 0
a=group:BUNDLE 0
a=extmap-allow-mixed
a=msid-semantic: WMS
m=application 9 UDP/DTLS/SCTP webrtc-datachannel
c=IN IP4 0.0.0.0
{0}
a=ice-ufrag:{1}
a=ice-pwd:{2}
a=ice-options:trickle
a=fingerprint:sha-256 {3}
a=setup:{4}
a=mid:0
a=sctp-port:5000
a=max-message-size:262144
";
    const string CANDIDATE_SKELETON =
@"a=candidate:{0} 1 udp {1} {2} {3} typ host generation 0 network-cost {4}";
    const string CANDIDATE_NET_SKELETON =
@"a=candidate:{0} 1 udp {1} {2} {3} typ host generation 0 network-id {5} network-cost {4}";
}
