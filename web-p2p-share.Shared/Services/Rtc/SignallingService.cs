using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace web_p2p_share.Shared.Services.Rtc;

public record RtcPeer(
    string Id,
    string Username);
public record ConnectionRequest(
    RtcPeer Peer,
    string Sdp);

public interface ISignallingService
{
    Task AdvertiseAsync(RtcPeer self);
    Task<RtcPeer[]> ListAsync();
    Task CreateConnectionAsync(
        RtcPeer self,
        RtcPeer target,
        string sdp);
    Task DeleteConnectionAsync(
        RtcPeer self,
        RtcPeer target);
    Task<string?> GetConnectionAnswerAsync(
        RtcPeer self,
        RtcPeer target);
    Task<ConnectionRequest[]> GetConnectionRequestsAsync(
        RtcPeer self);
    Task AnswerConnectionAsync(
        RtcPeer self,
        RtcPeer from,
        string sdp);
}

public class SignallingService : ISignallingService
{
    HttpClient m_http;

    public SignallingService(HttpClient http)
    {
        m_http = http;
    }

    public async Task<RtcPeer[]> ListAsync()
    {
        RtcPeerResponse[]? responses = await m_http.GetAsync(
            "/client/list",
            SignallingContext.Default.RtcPeerResponseArray);
        
        if (responses == null)
        {
            throw new Exception("Invalid server response");
        }

        return responses
            .Select(x =>
            {
                if (string.IsNullOrEmpty(x.Id) ||
                    string.IsNullOrEmpty(x.Username))
                {
                    throw new Exception("Invalid server response");
                }

                return new RtcPeer(x.Id, x.Username);
            })
            .ToArray();
    }

    public async Task AdvertiseAsync(RtcPeer self)
    {
        AddPeerRequest request = new AddPeerRequest()
        {
            Id = self.Id,
            Username = self.Username
        };
        await m_http.PostAsync(
            "/client/add",
            request,
            SignallingContext.Default.AddPeerRequest);
    }

    public async Task CreateConnectionAsync(RtcPeer self, RtcPeer target, string sdp)
    {
        CreateConnectionRequest request = new CreateConnectionRequest()
        {
            Id = self.Id,
            ToId = target.Id,
            Sdp = sdp
        };
        await m_http.PostAsync(
            "/connection/create",
            request,
            SignallingContext.Default.CreateConnectionRequest);
    }

    public async Task DeleteConnectionAsync(RtcPeer self, RtcPeer target)
    {
        DeleteConnectionRequest request = new DeleteConnectionRequest()
        {
            Id = self.Id,
            ToId = target.Id
        };
        await m_http.PostAsync(
            "/connection/delete",
            request,
            SignallingContext.Default.DeleteConnectionRequest);
    }

    public async Task<string?> GetConnectionAnswerAsync(RtcPeer self, RtcPeer target)
    {
        Dictionary<string, string> query = new Dictionary<string, string>()
        {
            { "id", self.Id },
            { "toId", target.Id }
        };
        GetAnswerResponse? response = await m_http.GetAsync(
            "/connection/get-answer",
            query,
            SignallingContext.Default.GetAnswerResponse);
        
        if (response == null)
        {
            throw new Exception("Invalid server response");
        }

        return response.Answer;
    }

    public async Task<ConnectionRequest[]> GetConnectionRequestsAsync(RtcPeer self)
    {
        Dictionary<string, string> query = new Dictionary<string, string>()
        {
            { "id", self.Id }
        };
        ConnectionRequestResponse[]? requests = await m_http.GetAsync(
            "/connection/get-requests",
            query,
            SignallingContext.Default.ConnectionRequestResponseArray);
        
        if (requests == null)
        {
            throw new Exception("Invalid server response");
        }

        return requests
            .Select(x =>
            {
                if (string.IsNullOrEmpty(x.FromId) ||
                    string.IsNullOrEmpty(x.FromUsername) ||
                    string.IsNullOrEmpty(x.Sdp))
                {
                    throw new Exception("Invalid server response");
                }

                return new ConnectionRequest(
                    new RtcPeer(x.FromId, x.FromUsername),
                    x.Sdp);
            })
            .ToArray();
    }

    public async Task AnswerConnectionAsync(RtcPeer self, RtcPeer from, string sdp)
    {
        SetAnswerRequest request = new SetAnswerRequest()
        {
            Id = self.Id,
            FromId = from.Id,
            Sdp = sdp
        };
        await m_http.PostAsync(
            "/connection/answer",
            request,
            SignallingContext.Default.SetAnswerRequest);
    }

    public record RtcPeerResponse
    {
        [JsonPropertyName("id")]
        public string? Id
        {
            get;
            set;
        }

        [JsonPropertyName("username")]
        public string? Username
        {
            get;
            set;
        }
    }

    public record AddPeerRequest
    {
        [JsonPropertyName("id")]
        public string Id
        {
            get;
            set;
        }

        [JsonPropertyName("username")]
        public string Username
        {
            get;
            set;
        }

        public AddPeerRequest()
        {
            Id = "";
            Username = "";
        }
    }

    public record CreateConnectionRequest
    {
        [JsonPropertyName("id")]
        public string Id
        {
            get;
            set;
        }

        [JsonPropertyName("toid")]
        public string ToId
        {
            get;
            set;
        }

        [JsonPropertyName("sdp")]
        public string Sdp
        {
            get;
            set;
        }

        public CreateConnectionRequest()
        {
            Id = "";
            ToId = "";
            Sdp = "";
        }
    }

    public record DeleteConnectionRequest
    {
        [JsonPropertyName("id")]
        public string Id
        {
            get;
            set;
        }

        [JsonPropertyName("toId")]
        public string ToId
        {
            get;
            set;
        }

        public DeleteConnectionRequest()
        {
            Id = "";
            ToId = "";
        }
    }

    public record GetAnswerResponse
    {
        [JsonPropertyName("answer")]
        public string? Answer
        {
            get;
            set;
        }
    }

    public record ConnectionRequestResponse
    {
        [JsonPropertyName("fromId")]
        public string? FromId
        {
            get;
            set;
        }

        [JsonPropertyName("fromUsername")]
        public string? FromUsername
        {
            get;
            set;
        }

        [JsonPropertyName("sdp")]
        public string? Sdp
        {
            get;
            set;
        }
    }

    public record SetAnswerRequest
    {
        [JsonPropertyName("id")]
        public string Id
        {
            get;
            set;
        }

        [JsonPropertyName("fromId")]
        public string FromId
        {
            get;
            set;
        }

        [JsonPropertyName("sdp")]
        public string Sdp
        {
            get;
            set;
        }

        public SetAnswerRequest()
        {
            Id = "";
            FromId = "";
            Sdp = "";
        }
    }
}

[JsonSerializable(typeof(SignallingService.RtcPeerResponse[]))]
[JsonSerializable(typeof(SignallingService.AddPeerRequest))]
[JsonSerializable(typeof(SignallingService.CreateConnectionRequest))]
[JsonSerializable(typeof(SignallingService.DeleteConnectionRequest))]
[JsonSerializable(typeof(SignallingService.GetAnswerResponse))]
[JsonSerializable(typeof(SignallingService.ConnectionRequestResponse[]))]
[JsonSerializable(typeof(SignallingService.SetAnswerRequest))]
internal partial class SignallingContext : JsonSerializerContext
{
}
