using System;

namespace web_p2p_share.Shared.Services.Rtc;

public class QrConnectionState
{
    IRtcConnection? m_connection;
    string? m_offer;
    string? m_answer;

    public Action? StateChanged;

    public IRtcConnection? Connection
    {
        get => m_connection;
        set
        {
            m_connection = value;
            StateChanged?.Invoke();
        }
    }

    public string? Offer
    {
        get => m_offer;
        set
        {
            m_offer = value;
            StateChanged?.Invoke();
        }
    }

    public string? Answer
    {
        get => m_answer;
        set
        {
            m_answer = value;
            StateChanged?.Invoke();
        }
    }
}
