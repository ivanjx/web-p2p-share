using System;
using System.Threading;

namespace web_p2p_share.Shared.Services.Rtc;

public record ReceiverConnection(
    RtcPeer Receiver,
    IRtcConnection? Connection,
    CancellationTokenSource? CreateConnectionCancellationTokenSource);

public record SenderConnection(
    ConnectionRequest Request,
    IRtcConnection? Connection,
    CancellationTokenSource? AnswerConnectionCancellationTokenSource);

public class ConnectionState
{
    ReceiverConnection[] m_receivers;
    SenderConnection[] m_senders;

    public Action? ReceiversChanged;
    public Action? SendersChanged;

    public ReceiverConnection[] Receivers
    {
        get => m_receivers;
        set
        {
            m_receivers = value;
            ReceiversChanged?.Invoke();
        }
    }

    public SenderConnection[] Senders
    {
        get => m_senders;
        set
        {
            m_senders = value;
            SendersChanged?.Invoke();
        }
    }

    public CancellationTokenSource? DiscoveryCancellationTokenSource
    {
        get;
        set;
    }

    public ConnectionState()
    {
        m_receivers = Array.Empty<ReceiverConnection>();
        m_senders = Array.Empty<SenderConnection>();
    }
}
