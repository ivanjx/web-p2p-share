using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using web_p2p_share.Shared.Services.Account;

namespace web_p2p_share.Shared.Services.Rtc;

public interface IConnectionService
{
    Task DiscoverReceiverAsync();
    Task DiscoverSenderAsync();
    void ConnectToReceiver(RtcPeer receiver);
    void CancelConnectToReceiver(RtcPeer receiver);
    void ReceiveConnectionRequest(RtcPeer sender);
    void CancelAnswerToSender(RtcPeer sender);
}

public class ConnectionService : IConnectionService
{
    ConnectionState m_state;
    AccountState m_accountState;
    IRtcService m_rtcService;
    ISignallingService m_signallingService;

    public ConnectionService(
        ConnectionState state,
        AccountState accountState,
        IRtcService rtcService,
        ISignallingService signallingService)
    {
        m_state = state;
        m_accountState = accountState;
        m_rtcService = rtcService;
        m_signallingService = signallingService;
    }

    public async Task DiscoverReceiverAsync()
    {
        RtcPeer[] newReceivers;

        try
        {
            Console.WriteLine("Advertising self");
            RtcPeer self = new RtcPeer(
                m_accountState.Id,
                m_accountState.Username);
            await m_signallingService.AdvertiseAsync(self);

            Console.WriteLine("Retrieving peer list");
            newReceivers = await m_signallingService.ListAsync();
            newReceivers = newReceivers
                .Where(x => x.Id != self.Id) // Exclude self.
                .ToArray();
        }
        catch (Exception ex)
        {
            Console.WriteLine("Unable to communicate with signalling server: {0}", ex);
            newReceivers = new RtcPeer[0];
        }

        RtcPeer[] existingPeers = m_state.Receivers
            .Select(x => x.Receiver)
            .ToArray();
        
        if (existingPeers.SequenceEqual(newReceivers))
        {
            // List already equal.
            return;
        }

        Console.WriteLine("Updating peer list");
        ReceiverConnection[] oldReceivers = m_state.Receivers;
        m_state.Receivers = newReceivers
            .Select(newX =>
            {
                ReceiverConnection? existingConnection = m_state.Receivers
                    .Where(x => x.Receiver.Id == newX.Id)
                    .FirstOrDefault();
                return new ReceiverConnection(
                    newX,
                    existingConnection?.Connection,
                    existingConnection?.CreateConnectionCancellationTokenSource);
            })
            .ToArray();
        
        foreach (ReceiverConnection oldReceiver in oldReceivers)
        {
            if (oldReceiver.Connection != null &&
                !oldReceiver.Connection.IsOpen)
            {
                Console.WriteLine("Disposing old receiver connection: {0}", oldReceiver.Receiver);
                oldReceiver.Connection.Dispose();
                oldReceivers = oldReceivers
                    .Where(x => x.Receiver.Id != oldReceiver.Receiver.Id)
                    .ToArray();
            }
        }

        // Old receivers now only contains active connections.
        oldReceivers = oldReceivers
            .Where(x => !newReceivers // not added to new state yet
                .Select(n => n.Id)
                .Contains(x.Receiver.Id))
            .ToArray();
        
        if (oldReceivers.Length > 0)
        {
            Console.WriteLine("Merging old active connections");
            m_state.Receivers = m_state.Receivers
                .Concat(oldReceivers)
                .ToArray();
        }
    }

    public async Task DiscoverSenderAsync()
    {
        ConnectionRequest[] newRequests;

        try
        {
            Console.WriteLine("Advertising self");
            RtcPeer self = new RtcPeer(
                m_accountState.Id,
                m_accountState.Username);
            await m_signallingService.AdvertiseAsync(self);

            Console.WriteLine("Retrieving request list");
            newRequests = await m_signallingService.GetConnectionRequestsAsync(self);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Unable to communicate with signalling server: {0}", ex);
            newRequests = new ConnectionRequest[0];
        }

        ConnectionRequest[] existingRequests = m_state.Senders
            .Select(x => x.Request)
            .ToArray();

        if (existingRequests.SequenceEqual(newRequests))
        {
            // List already equal.
            return;
        }

        Console.WriteLine("Updating request list");
        SenderConnection[] oldSenders = m_state.Senders;
        m_state.Senders = newRequests
            .Select(newX =>
            {
                SenderConnection? existingConnection = m_state.Senders
                    .Where(x => x.Request.Peer.Id == newX.Peer.Id)
                    .FirstOrDefault();
                return new SenderConnection(
                    newX,
                    existingConnection?.Connection,
                    existingConnection?.AnswerConnectionCancellationTokenSource);
            })
            .ToArray();
        
        foreach (SenderConnection oldSender in oldSenders)
        {
            if (oldSender.Connection != null &&
                !oldSender.Connection.IsOpen)
            {
                Console.WriteLine("Disposing old sender connection: {0}", oldSender.Request.Peer);
                oldSender.Connection.Dispose();
                oldSenders = oldSenders
                    .Where(x => x.Request.Peer.Id != oldSender.Request.Peer.Id)
                    .ToArray();
            }
        }

        // Old senders now only contains active connections.
        oldSenders = oldSenders
            .Where(x => !newRequests // not added to new state yet.
                .Select(n => n.Peer.Id)
                .Contains(x.Request.Peer.Id))
            .ToArray();

        if (oldSenders.Length > 0)
        {
            Console.WriteLine("Merging old active connections");
            m_state.Senders = m_state.Senders
                .Concat(oldSenders)
                .ToArray();
        }
    }

    public async void ConnectToReceiver(RtcPeer receiver)
    {
        ReceiverConnection? connection = m_state.Receivers
            .Where(x => x.Receiver == receiver)
            .FirstOrDefault();
        
        if (connection == null)
        {
            // Impossible.
            return;
        }

        if (connection.Connection != null ||
            connection.CreateConnectionCancellationTokenSource != null)
        {
            Console.WriteLine("Already connected to receiver: {0}", receiver);
            return;
        }

        Console.WriteLine("Creating rtc connection");
        IRtcConnection rtc = await m_rtcService.StartConnectionAsync();

        Console.WriteLine("Updating connection state");
        using CancellationTokenSource cts = new CancellationTokenSource();
        m_state.Receivers = m_state.Receivers
            .Select(x =>
            {
                if (x.Receiver.Id == receiver.Id)
                {
                    return x with
                    {
                        CreateConnectionCancellationTokenSource = cts
                    };
                }

                return x;
            })
            .ToArray();
        

        try
        {
            Console.WriteLine("Sending connect request to receiver: {0}", receiver);
            RtcPeer self = new RtcPeer(
                m_accountState.Id,
                m_accountState.Username);
            await m_signallingService.CreateConnectionAsync(
                self,
                receiver,
                rtc.Sdp);
            
            while (true)
            {
                cts.Token.ThrowIfCancellationRequested();

                Console.WriteLine("Acquiring connection state with receiver: {0}", receiver);
                string? answer = await m_signallingService.GetConnectionAnswerAsync(self, receiver);

                if (answer == null)
                {
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    continue;
                }

                Console.WriteLine("Got answer from receiver: {0}", receiver);
                await m_rtcService.SetConnectionAnswerAsync(rtc, answer);
                break;
            }

            Console.WriteLine("Waiting for connection with receiver: {0}", receiver);

            while (!rtc.IsOpen)
            {
                cts.Token.ThrowIfCancellationRequested();
                await Task.Delay(TimeSpan.FromSeconds(1));
            }

            Console.WriteLine("Connected to receiver: {0}", receiver);
            m_state.Receivers = m_state.Receivers
                .Select(x =>
                {
                    if (x.Receiver.Id == receiver.Id)
                    {
                        return x with
                        {
                            Connection = rtc
                        };
                    }

                    return x;
                })
                .ToArray();
        }
        catch (Exception ex)
        {
            Console.WriteLine("ConnectToReceiver error: {0}", ex);
            rtc.Dispose();
        }
        finally
        {
            // Removing cancel token.
            Console.WriteLine("Updating connection state");
            m_state.Receivers = m_state.Receivers
                .Select(x =>
                {
                    if (x.Receiver.Id == receiver.Id &&
                        x.CreateConnectionCancellationTokenSource != null)
                    {
                        return x with
                        {
                            CreateConnectionCancellationTokenSource = null
                        };
                    }

                    return x;
                })
                .ToArray();
        }
    }

    public async void CancelConnectToReceiver(RtcPeer receiver)
    {
        ReceiverConnection? connection = m_state.Receivers
            .Where(x => x.Receiver == receiver)
            .FirstOrDefault();
        
        if (connection == null ||
            connection.CreateConnectionCancellationTokenSource == null)
        {
            return;
        }

        Console.WriteLine("Cancelling connection attempt to receiver: {0}", receiver);
        connection.CreateConnectionCancellationTokenSource.Cancel();

        try
        {
            Console.WriteLine("Deleting connection request to receiver: {0}", receiver);
            RtcPeer self = new RtcPeer(
                m_accountState.Id,
                m_accountState.Username);
            await m_signallingService.DeleteConnectionAsync(self, receiver);
        }
        catch (Exception ex)
        {
            Console.WriteLine("CancelConnectToReceiver error: {0}", ex);
        }
    }

    public async void ReceiveConnectionRequest(RtcPeer sender)
    {
        SenderConnection? connection = m_state.Senders
            .Where(x => x.Request.Peer.Id == sender.Id)
            .FirstOrDefault();
        
        if (connection == null)
        {
            // Impossible.
            return;
        }

        if (connection.Connection != null ||
            connection.AnswerConnectionCancellationTokenSource != null)
        {
            Console.WriteLine("Already connected to sender: {0}", sender);
            return;
        }

        Console.WriteLine("Creating rtc connection for sender: {0}", sender);
        IRtcConnection rtc = await m_rtcService.ReceiveConnectionAsync(
            connection.Request.Sdp);
        
        Console.WriteLine("Updating connection state");
        using CancellationTokenSource cts = new CancellationTokenSource();
        m_state.Senders = m_state.Senders
            .Select(x =>
            {
                if (x.Request.Peer.Id == sender.Id)
                {
                    return x with
                    {
                        AnswerConnectionCancellationTokenSource = cts
                    };
                }

                return x;
            })
            .ToArray();

        try
        {
            Console.WriteLine("Sending answer to sender: {0}");
            RtcPeer self = new RtcPeer(
                m_accountState.Id,
                m_accountState.Username);
            await m_signallingService.AnswerConnectionAsync(
                self,
                sender,
                rtc.Sdp);
            
            Console.WriteLine("Waiting for connection with sender: {0}", sender);

            while (!rtc.IsOpen)
            {
                cts.Token.ThrowIfCancellationRequested();
                await Task.Delay(TimeSpan.FromSeconds(1));
            }

            Console.WriteLine("Connected with sender: {0}", sender);
            m_state.Senders = m_state.Senders
                .Select(x =>
                {
                    if (x.Request.Peer.Id == sender.Id)
                    {
                        return x with
                        {
                            Connection = rtc
                        };
                    }

                    return x;
                })
                .ToArray();
        }
        catch (Exception ex)
        {
            Console.WriteLine("ReceiveConnectionRequest error: {0}", ex);
            rtc.Dispose();
        }
        finally
        {
            // Removing cancel token.
            Console.WriteLine("Updating connection state");
            m_state.Senders = m_state.Senders
                .Select(x =>
                {
                    if (x.Request.Peer.Id == sender.Id &&
                        x.AnswerConnectionCancellationTokenSource != null)
                    {
                        return x with
                        {
                            AnswerConnectionCancellationTokenSource = null
                        };
                    }

                    return x;
                })
                .ToArray();
        }
    }

    public void CancelAnswerToSender(RtcPeer sender)
    {
        SenderConnection? connection = m_state.Senders
            .Where(x => x.Request.Peer.Id == sender.Id)
            .FirstOrDefault();
        
        if (connection == null ||
            connection.AnswerConnectionCancellationTokenSource == null)
        {
            return;
        }

        Console.WriteLine("Cancelling answer attempt to sender: {0}", sender);
        connection.AnswerConnectionCancellationTokenSource.Cancel();
    }
}
