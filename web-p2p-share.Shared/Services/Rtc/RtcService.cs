using System;
using System.Threading.Tasks;

namespace web_p2p_share.Shared.Services.Rtc;

public interface IRtcConnection : IDisposable
{
    bool IsOpen
    {
        get;
    }

    string Sdp
    {
        get;
    }
}

public interface IRtcService
{
    Task<IRtcConnection> StartConnectionAsync();
    Task SetConnectionAnswerAsync(
        IRtcConnection connection,
        string remoteSdp);
    Task<IRtcConnection> ReceiveConnectionAsync(string remoteSdp);
    ValueTask<byte[]> ReadAsync(
        IRtcConnection connection,
        bool isCommand,
        int count);
    ValueTask WriteAsync(
        IRtcConnection connection,
        bool isCommand,
        Memory<byte> data);
}
