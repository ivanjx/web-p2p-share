using System;
using System.Threading;
using System.Threading.Tasks;

namespace web_p2p_share.Shared.Services.Rtc;

public interface IDiscoveryTaskService
{
    void StartSender();
    void StartReceiver();
    void Stop();
}

public class DiscoveryTaskService : IDiscoveryTaskService
{
    ConnectionState m_connectionState;
    IConnectionService m_connectionService;

    public DiscoveryTaskService(
        ConnectionState connectionState,
        IConnectionService connectionService)
    {
        m_connectionState = connectionState;
        m_connectionService = connectionService;
    }

    public async void StartReceiver()
    {
        if (m_connectionState.DiscoveryCancellationTokenSource != null)
        {
            return;
        }

        m_connectionState.DiscoveryCancellationTokenSource = new CancellationTokenSource();
        CancellationToken cancellationToken = m_connectionState.DiscoveryCancellationTokenSource.Token;

        while (!cancellationToken.IsCancellationRequested)
        {
            await m_connectionService.DiscoverSenderAsync();
            
            try
            {
                await Task.Delay(
                    TimeSpan.FromSeconds(1),
                    cancellationToken);
            }
            catch { }
        }
    }

    public async void StartSender()
    {
        if (m_connectionState.DiscoveryCancellationTokenSource != null)
        {
            return;
        }

        m_connectionState.DiscoveryCancellationTokenSource = new CancellationTokenSource();
        CancellationToken cancellationToken = m_connectionState.DiscoveryCancellationTokenSource.Token;

        while (!cancellationToken.IsCancellationRequested)
        {
            await m_connectionService.DiscoverReceiverAsync();
            
            try
            {
                await Task.Delay(
                    TimeSpan.FromSeconds(1),
                    cancellationToken);
            }
            catch { }
        }
    }

    public void Stop()
    {
        Console.WriteLine("Stopping discovery task");
        m_connectionState.DiscoveryCancellationTokenSource?.Cancel();
        m_connectionState.DiscoveryCancellationTokenSource?.Dispose();
        m_connectionState.DiscoveryCancellationTokenSource = null;
    }
}
