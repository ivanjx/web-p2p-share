using System;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using web_p2p_share.Shared.Services.Account;

namespace web_p2p_share.Shared.Services.Rtc;

public interface IQrConnectionService
{
    Task CreateOfferAsync();
    Task WaitAnswerAsync();
    Task WaitOfferAsync();
    void Cancel();
}

public class QrConnectionService : IQrConnectionService
{
    QrConnectionState m_state;
    AccountState m_accountState;
    ConnectionState m_connectionState;
    IRtcService m_rtcService;
    ISdpService m_sdpService;

    public QrConnectionService(
        QrConnectionState state,
        AccountState accountState,
        ConnectionState connectionState,
        IRtcService rtcService,
        ISdpService sdpService)
    {
        m_state = state;
        m_accountState = accountState;
        m_connectionState = connectionState;
        m_rtcService = rtcService;
        m_sdpService = sdpService;
    }

    public async Task CreateOfferAsync()
    {
        if (m_state.Connection != null)
        {
            return; // Impossible.
        }

        try
        {
            Console.WriteLine("Creating new rtc connection");
            m_state.Connection = await m_rtcService.StartConnectionAsync();
            m_state.Offer = m_sdpService.Compress(m_state.Connection.Sdp);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Unable to create rtc connection: {0}", ex);
            m_state.Connection?.Dispose();
            m_state.Connection = null;
        }
    }

    public async Task WaitAnswerAsync()
    {
        Console.WriteLine("Waiting for sdp answer");
        string sdpAnswer;

        while (true)
        {
            if (m_state.Connection == null)
            {
                Console.WriteLine("Wait sdp answer cancelled");
                return;
            }

            if (m_state.Answer == null)
            {
                // No answer yet.
                await Task.Delay(TimeSpan.FromSeconds(1));
                continue;
            }

            try
            {
                Console.WriteLine("Decompressing answer: {0}", m_state.Answer);
                sdpAnswer = m_sdpService.Decompress(m_state.Answer);
                break;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to decompress answer: {0}", ex);
                m_state.Answer = null;
                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }

        try
        {
            Console.WriteLine("Setting connection answer");
            await m_rtcService.SetConnectionAnswerAsync(
                m_state.Connection,
                sdpAnswer);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Unable to accept rtc answer: {0}", ex);
            m_state.Connection.Dispose();
            m_state.Connection = null;
            m_state.Answer = null;
            return; // Retry from beginning.
        }

        Console.WriteLine("Waiting for connection to receiver");

        while (true)
        {
            if (m_state.Connection == null)
            {
                Console.WriteLine("Connection request cancelled");
                m_state.Answer = null;
                return;
            }

            if (m_state.Connection.IsOpen)
            {
                break;
            }

            await Task.Delay(TimeSpan.FromSeconds(1));
        }

        try
        {
            Console.WriteLine("Sending account info");
            PeerInfo selfInfo = new PeerInfo()
            {
                Id = m_accountState.Id,
                Username = m_accountState.Username
            };
            string selfInfoStr = JsonSerializer.Serialize(
                selfInfo,
                QrConnectionContext.Default.PeerInfo);
            byte[] selfInfoBuff = Encoding.Unicode.GetBytes(selfInfoStr);
            await m_rtcService.WriteAsync(
                m_state.Connection,
                true,
                BitConverter.GetBytes(selfInfoBuff.Length));
            await m_rtcService.WriteAsync(
                m_state.Connection,
                true,
                selfInfoBuff);
            
            Console.WriteLine("Reading receiver info");
            byte[] infoLenBuff = await m_rtcService.ReadAsync(
                m_state.Connection,
                true,
                4);
            int infoLen = BitConverter.ToInt32(infoLenBuff);
            byte[] infoBuff = await m_rtcService.ReadAsync(
                m_state.Connection,
                true,
                infoLen);
            PeerInfo? receiverInfo = JsonSerializer.Deserialize(
                Encoding.Unicode.GetString(infoBuff),
                QrConnectionContext.Default.PeerInfo);
            
            if (receiverInfo == null ||
                string.IsNullOrEmpty(receiverInfo.Id) ||
                string.IsNullOrEmpty(receiverInfo.Username))
            {
                throw new Exception("Invalid sender info");
            }

            ReceiverConnection? connection = m_connectionState.Receivers
                .Where(x => x.Receiver.Id == receiverInfo.Id)
                .FirstOrDefault();
            
            if (connection != null)
            {
                throw new Exception("Peer already connected: " + receiverInfo.Id);
            }

            Console.WriteLine("Adding receiver to connection state: {0}", receiverInfo);
            connection = new ReceiverConnection(
                new RtcPeer(receiverInfo.Id, receiverInfo.Username),
                m_state.Connection,
                null);
            m_connectionState.Receivers = m_connectionState.Receivers
                .Append(connection)
                .ToArray();
        }
        catch (Exception ex)
        {
            Console.WriteLine("Unable to exchange information with receiver: {0}", ex);
            m_state.Connection?.Dispose();
        }

        Console.WriteLine("Resetting qr connection state");
        m_state.Connection = null;
        m_state.Answer = null;
    }

    public async Task WaitOfferAsync()
    {
        Console.WriteLine("Waiting for sdp offer");
        m_state.Connection = await m_rtcService.StartConnectionAsync(); // Flag.
        string offerSdp;

        while (true)
        {
            if (m_state.Connection == null)
            {
                Console.WriteLine("Wait sdp offer cancelled");
                return;
            }

            if (m_state.Offer == null)
            {
                // No offer yet.
                await Task.Delay(TimeSpan.FromSeconds(1));
                continue;
            }

            try
            {
                Console.WriteLine("Decompressing offer: {0}", m_state.Offer);
                offerSdp = m_sdpService.Decompress(m_state.Offer);
                break;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Unable to decompress offer: {0}", ex);
                m_state.Offer = null;
                await Task.Delay(TimeSpan.FromSeconds(1));
            }
        }

        try
        {
            m_state.Connection.Dispose(); // Remove flag.

            Console.WriteLine("Creating rtc connection from offer");
            m_state.Connection = await m_rtcService.ReceiveConnectionAsync(offerSdp);
            m_state.Answer = m_sdpService.Compress(m_state.Connection.Sdp);
        }
        catch (Exception ex)
        {
            Console.WriteLine("Unable to accept rtc offer: {0}", ex);
            m_state.Connection?.Dispose();
            m_state.Connection = null;
            m_state.Offer = null;
        }

        Console.WriteLine("Waiting for connection to sender");

        while (true)
        {
            if (m_state.Connection == null)
            {
                Console.WriteLine("Connection request cancelled");
                m_state.Offer = null;
                m_state.Answer = null;
                return;
            }

            if (m_state.Connection.IsOpen)
            {
                break;
            }

            await Task.Delay(TimeSpan.FromSeconds(1));
        }

        try
        {
            Console.WriteLine("Reading sender info");
            byte[] infoLenBuff = await m_rtcService.ReadAsync(
                m_state.Connection,
                true,
                4);
            int infoLen = BitConverter.ToInt32(infoLenBuff);
            byte[] infoBuff = await m_rtcService.ReadAsync(
                m_state.Connection,
                true,
                infoLen);
            PeerInfo? senderInfo = JsonSerializer.Deserialize(
                Encoding.Unicode.GetString(infoBuff),
                QrConnectionContext.Default.PeerInfo);
            
            if (senderInfo == null ||
                string.IsNullOrEmpty(senderInfo.Id) ||
                string.IsNullOrEmpty(senderInfo.Username))
            {
                throw new Exception("Invalid sender info");
            }

            SenderConnection? connection = m_connectionState.Senders
                .Where(x => x.Request.Peer.Id == senderInfo.Id)
                .FirstOrDefault();
            
            if (connection != null)
            {
                throw new Exception("Peer already connected: " + senderInfo.Id);
            }

            Console.WriteLine("Sending account info");
            PeerInfo selfInfo = new PeerInfo()
            {
                Id = m_accountState.Id,
                Username = m_accountState.Username
            };
            string selfInfoStr = JsonSerializer.Serialize(
                selfInfo,
                QrConnectionContext.Default.PeerInfo);
            byte[] selfInfoBuff = Encoding.Unicode.GetBytes(selfInfoStr);
            await m_rtcService.WriteAsync(
                m_state.Connection,
                true,
                BitConverter.GetBytes(selfInfoBuff.Length));
            await m_rtcService.WriteAsync(
                m_state.Connection,
                true,
                selfInfoBuff);
            
            Console.WriteLine("Delaying for a bit");
            await Task.Delay(TimeSpan.FromSeconds(3));

            if (!m_state.Connection.IsOpen)
            {
                throw new Exception("Connection closed by sender: " + senderInfo.Id);
            }

            Console.WriteLine("Adding sender to connection state: {0}", senderInfo);
            connection = new SenderConnection(
                new ConnectionRequest(
                    new RtcPeer(senderInfo.Id, senderInfo.Username),
                    m_state.Connection.Sdp),
                m_state.Connection,
                null);
            m_connectionState.Senders = m_connectionState.Senders
                .Append(connection)
                .ToArray();
        }
        catch (Exception ex)
        {
            Console.WriteLine("Unable to exchange information with sender: {0}", ex);
            m_state.Connection?.Dispose();
        }

        Console.WriteLine("Resetting qr connection state");
        m_state.Connection = null;
        m_state.Offer = null;
        m_state.Answer = null;
    }

    public void Cancel()
    {
        if (m_state.Connection == null)
        {
            return;
        }

        m_state.Connection.Dispose();
        m_state.Connection = null;
    }

    public class PeerInfo
    {
        [JsonPropertyName("id")]
        public string? Id
        {
            get;
            set;
        }

        [JsonPropertyName("username")]
        public string? Username
        {
            get;
            set;
        }
    }
}

[JsonSerializable(typeof(QrConnectionService.PeerInfo))]
internal partial class QrConnectionContext : JsonSerializerContext
{
}
